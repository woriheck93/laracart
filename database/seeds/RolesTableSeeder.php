<?php


use App\Laracart\User\Entities\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
       	Role::create(['name' => 'admin', 'display_name' => 'Admin', 'description' => '']);
       	Role::create(['name' => 'store', 'display_name' => 'Store', 'description' => '']); 
    }
}
