<?php

use App\Laracart\User\Entities\Admin;
use App\Laracart\User\Entities\Store;
use App\Laracart\User\Entities\StoreDetail;


$factory->define(Admin::class, function (Faker\Generator $faker) {
    static $password;

    return [  
        'email' => $faker->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Store::class, function (Faker\Generator $faker) {
    static $password;

    return [  
        'email' => $faker->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->defineAs(Store::class, 'parameter', function (Faker\Generator $faker) {
    static $password;

    return [  
        'email' => $faker->email
    ];
});

$factory->define(StoreDetail::class, function (Faker\Generator $faker) {
    
    $faker->addProvider(new Faker\Provider\en_ZA\PhoneNumber($faker));
    $faker->addProvider(new Faker\Provider\en_US\Payment($faker));

    return [  
    	'user_id'	=>	factory(Store::class)->create()->id,
		'name'		=>	$faker->name,
		'phone' 	=> 	$faker->phoneNumber,
		'address'	=>	$faker->address,
		'account'	=>	$faker->bankAccountNumber, 
		'balance'	=> 	$faker->randomFloat + 0.01
    ];

});

$factory->defineAs(StoreDetail::class, 'parameter', function (Faker\Generator $faker) {
    
    $faker->addProvider(new Faker\Provider\en_ZA\PhoneNumber($faker));
    $faker->addProvider(new Faker\Provider\en_US\Payment($faker));

    return [
		'name'		=>	$faker->name,
		'phone' 	=> 	$faker->phoneNumber,
		'address'	=>	$faker->address,
		'account'	=>	$faker->bankAccountNumber, 
		'balance'	=> 	$faker->randomNumber + 0.01
    ];

});