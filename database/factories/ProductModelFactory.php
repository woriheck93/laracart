<?php

use App\Laracart\Product\Entities\Phone;

$factory->define(Phone::class, function (Faker\Generator $faker) { 

    return [  
		'name' => $faker->word,
		'base_price' => $faker->randomNumber + 0.01,
		'color' =>  [$faker->colorName,$faker->colorName,$faker->colorName],
		'size' =>  $faker->randomElements(['small','medium','large'])[0],
		'weight' => '100gram',
		'memory' => '10GB',
		'camera' => $faker->word,
		'capacity' => ['32GB', '64GB'],
		'display' => $faker->text,
		'battery' => $faker->text,
		'operating_system' =>  $faker->text,
		'cpu' => $faker->text,
		'input_output_port' => $faker->text,
		'wireless_technology' =>  $faker->text,
		'remark' => $faker->text,
    ];
});

