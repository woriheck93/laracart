const elixir = require('laravel-elixir');

//require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.styles([
        'uikit.gradient.css',
        'sweetalert.css'
    ], './public/css/admin/util.css')
})
elixir(mix => {
    mix.sass(['login.scss'], './public/css/admin/login.css');
})
elixir(mix => {
    mix.sass(['admin.scss'], './public/css/admin/admin.css');
})
elixir(mix => {
    mix.webpack(['admin/login.js'], './public/js/admin/login.js');
})
elixir(mix => {
    mix.webpack(['admin/index.js'], './public/js/admin/index.js');
})
