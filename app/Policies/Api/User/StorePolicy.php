<?php

namespace App\Policies\Api\User;

use App\Laracart\User\Entities\Store;
use App\Laracart\User\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StorePolicy
{
    
    use HandlesAuthorization;

    public function before($user)
    {
        if( $user->hasRole('admin') ) return true;
    } 

     /**
     * Determine whether the user can edit the store.
     *
     * @param  \App\User  $user
     * @param  \App\Store  $store
     * @return mixed
     */
    public function edit(User $user, Store $store)
    {
        return $user->id === $store->id;
    } 
    /**
     * Determine whether the user can update the store.
     *
     * @param  \App\User  $user
     * @param  \App\Store  $store
     * @return mixed
     */
    public function update(User $user, Store $store)
    {
        return $user->id === $store->id;
    } 
 

}
