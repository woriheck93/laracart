<?php 

namespace App\Laracart\Transformer;

abstract class Transformer {
	
	/**
	* Transform a collection of items
	*
	* @param $items
	* @return array
	*/
	
	public function transformCollection($items)
	{
		return array_map([$this, 'transform'],$items);
	}

	public function transformCollectionWithKey($items)
	{
		return collect($this->transformCollection($items))->keyBy('id');
	}
	
}
