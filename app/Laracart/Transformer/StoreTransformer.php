<?php

namespace App\Laracart\Transformer;

use App\Laracart\Transformer\Transformer;


class StoreTransformer extends Transformer
{	
	public function transform($store)
	{
		$store->toArray();
		return [ 
			'id' =>  $store['id'],
			'name'=> $store['details']['name'],
            'email'=> $store['email'],
            'phone'=> $store['details']['phone'],
            'address' => $store['details']['address'], 
            'account' => $store['details']['account'],
            'balance' => $store['details']['balance']
		]; 
	}
	public function show($store)
	{   
		return [ 
			'id' => $store['id'],
			'name'=> $store['details']['name'],
            'email'=> $store['email'],
            'phone'=> $store['details']['phone'],
            'address' => $store['details']['address'], 
            'account' => $store['details']['account'],
            'balance' => $store['details']['balance']
		]; 
	}
	public function store($store)
	{  
		return [ 
			'id' => $store['id'],
			'name'=> $store['name'],
            'email'=> $store['email'],
            'phone'=> $store['phone'],
            'address' => $store['address'], 
            'account' => $store['account'],
            'balance' => $store['balance']
		]; 
	} 
	public function update($store)
	{  
		return [ 
			'name'=> $store['name'], 
            'phone'=> $store['phone'],
            'address' => $store['address'], 
            'account' => $store['account'],
            'balance' => $store['balance']
		]; 
	}
}