<?php

namespace App\Laracart\Eloquent\Contracts;

interface RepositoryInterface
{
	/**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);
}