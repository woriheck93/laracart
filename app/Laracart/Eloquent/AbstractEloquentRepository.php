<?php

namespace App\Laracart\Eloquent;

use App\Laracart\Eloquent\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Exception;
class AbstractEloquentRepository implements RepositoryInterface
{
	/**
	* 
    * @var model
    */
    protected $model;

	public function __construct(Model $model)
    {
        $this->model = $model; 
    }

    /**
     * @param integer $id
     * @return self
     */

    public function find($id)
    {
        return $this->model->find($id); 
    } 

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes); 
    } 

    /**
     * @param array $attributes
     * @return mixed
     */
    public function update(array $attributes)
    {  
        return $this->model->find($attributes['id'])->update($attributes);   
    } 
    

}
