<?php

namespace App\Laracart\Product\Entities;

use Moloquent;

class Product extends Moloquent{

	protected $connection = 'mongodb';

    protected $collection = 'products_collection';

}