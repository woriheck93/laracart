<?php

namespace App\Laracart\Product\Entities;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Moloquent;

class Phone extends Moloquent implements StaplerableInterface {
    
    use EloquentTrait;

	protected $connection = 'mongodb';

    protected $collection = 'phones_collection'; 

    public function __construct(array $attributes = array()) 
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }

    protected $fillable= [
    	'image',
    	'name',
		'base_price',
		'color',
		'size',
		'weight',
		'memory',
		'camera',
		'capacity',
		'display',
		'battery',
		'operating_system',
		'cpu',
		'input_output_port',
		'wireless_technology',
		'remark'
    ];

    public function scopeBasicDetails($query){
    	return $query->select(['_id','name','base_price','color','size','weight','memory','capacity']);
    }

}