<?php

namespace App\Laracart\Product\Repositories;

use App\Laracart\Eloquent\AbstractEloquentRepository; 
use App\Laracart\Product\Entities\Phone;

class PhoneRepository extends AbstractEloquentRepository
{	  
    /**
    * Create a phone model instance
    *
    * @param App\Laracart\Product\Entities\Phone
    */
    public function __construct(Phone $model)
    {
        $this->model = $model;
    }

    public function getBasicDetailsLists()
    {
    	return $this->model->basicDetails()->paginate();
    }

    public function getSinglePhone($id)
    {
        return $this->model->find($id)->attributesToArray();
    }

}
