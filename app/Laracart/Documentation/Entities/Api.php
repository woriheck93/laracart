<?php

namespace App\Laracart\Documentation\Entities;

use Moloquent;


class Api extends Moloquent
{  
	
	protected $connection = 'mongodb';

    protected $collection = 'api_collection'; 

}
