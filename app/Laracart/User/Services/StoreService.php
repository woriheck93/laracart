<?php

namespace App\Laracart\User\Services;

use App\Laracart\Transformer\StoreTransformer;
use App\Laracart\User\Repositories\StoreDetailRepository;
use App\Laracart\User\Repositories\StoreRepository;


class StoreService
{
	protected $storeRepository;
    protected $storeTransformer;
    protected $storeDetailRepository;
	
	function __construct(StoreRepository $storeRepository,StoreDetailRepository $storeDetailRepository,StoreTransformer $storeTransformer)
	{
        $this->storeRepository = $storeRepository;
        $this->storeTransformer = $storeTransformer;
        $this->storeDetailRepository = $storeDetailRepository;
	}

	
	
}