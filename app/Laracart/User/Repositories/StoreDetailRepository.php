<?php

namespace App\Laracart\User\Repositories;

use App\Laracart\Eloquent\AbstractEloquentRepository;
use App\Laracart\User\Entities\StoreDetail;
 
class StoreDetailRepository extends AbstractEloquentRepository
{	  
 
    
    /** 
    *  @param App\Laracart\User\Entities\storeDetail
    *  
    */

    public function __construct(StoreDetail $model)
    {
        $this->model = $model; 
    } 

    /**
     *
     * @param array 
     * @return boolean
     */

    public function updateByUserId(array $attributes)
    {
    	return  $this->model->where('user_id',$attributes['user_id'])
                            ->first()
                            ->update($attributes);	
    }

}
