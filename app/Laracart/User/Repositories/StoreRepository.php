<?php

namespace App\Laracart\User\Repositories;

use App\Laracart\Eloquent\AbstractEloquentRepository;
use App\Laracart\User\Entities\Role;
use App\Laracart\User\Entities\Store;
use App\Laracart\User\Entities\StoreDetail;
use Exception;

class StoreRepository extends AbstractEloquentRepository
{	  
    /**
    *  @param App\Laracart\User\Entities\Store
    *  Constructor
    */

    public function __construct(Store $model)
    {
        $this->model = $model; 
    } 

    public function create(array $data)
    { 
        $store = $this->model->create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        $store->attachRole(Role::name('store')); 
        return $store;
    }

    public function getStoresDetailLists()
    {
        return $this->model->select('id','email','role')->with(['details' => function($query){
            return $query->select('user_id','name','phone','address','account','balance');
        }])->paginate(); 
        
    }

    public function getStoreDetail($id)
    {
        return $this->model->whereId($id)->with('details')->first()->toArray(); 
    }  
}
