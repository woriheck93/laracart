<?php

namespace App\Laracart\User\Entities;
 
use Illuminate\Database\Eloquent\Model;

class StoreDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','name', 'phone', 'address','account','balance',
    ];

    public function store()
    {
        return $this->belongsTo('App\Laracart\User\Entities\Store','user_id');
    }
    
}
