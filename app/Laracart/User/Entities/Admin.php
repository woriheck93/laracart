<?php

namespace App\Laracart\User\Entities;

use App\Laracart\User\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Admin extends User
{
    protected static $singleTableType = 'admin';
}
