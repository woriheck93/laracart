<?php

namespace App\Laracart\User\Entities;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public static function name($name)
    {
    	return Role::whereName($name)->first();
    }
}
