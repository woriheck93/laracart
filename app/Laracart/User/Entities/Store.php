<?php

namespace App\Laracart\User\Entities;

use App\Laracart\User\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Store extends User
{
    protected static $singleTableType = 'store'; 

    public function details()
    {
        return $this->hasOne('App\Laracart\User\Entities\StoreDetail','user_id');
    }
}
