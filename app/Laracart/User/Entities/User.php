<?php

namespace App\Laracart\User\Entities;

use App\Laracart\User\Entities\Admin;
use App\Laracart\User\Entities\Store; 
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, SingleTableInheritanceTrait;

    protected $table = 'users';

    protected static $singleTableTypeField = 'role';

    protected static $singleTableSubclasses = [Admin::class, Store::class]; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
