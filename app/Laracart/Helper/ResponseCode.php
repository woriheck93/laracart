<?php

namespace App\Laracart\Helper;
use Illuminate\Http\Response as HttpResponse;

class ResponseCode
{ 
    const USER_NOT_FOUND = 100;
    const INVALID_LOGIN = 101;
    const INVALID_ROLE = 102;
    const TOKEN_EXPIRED = 103;
    const TOKEN_INVALID = 104;
    const PERMISSION_DENIED = 105;
    const TOKEN_NOT_PROVIDED = 106;
    const FORM_VALIDATE_FAILED = 300;

    public static $statusTexts = [
        self::USER_NOT_FOUND => 'Undefined user.',
        self::INVALID_LOGIN => 'Login Failed.',
        self::INVALID_ROLE	=> 'Permission denied, your role are not authorize to do this action.',
        self::TOKEN_EXPIRED => 'Token expired, relogin again.',
        self::TOKEN_INVALID => 'Token invalid, request failed.',
        self::TOKEN_NOT_PROVIDED => 'Request failed, token not provided.',
        self::PERMISSION_DENIED => 'Permission denied, you are not authorize to do this action.',
    ];

     public static $httpCode = [
        self::USER_NOT_FOUND => HttpResponse::HTTP_BAD_REQUEST,
        self::INVALID_LOGIN => HttpResponse::HTTP_UNAUTHORIZED,
        self::INVALID_ROLE  => HttpResponse::HTTP_FORBIDDEN,
        self::TOKEN_EXPIRED => HttpResponse::HTTP_FORBIDDEN,
        self::TOKEN_INVALID => HttpResponse::HTTP_UNAUTHORIZED,
        self::FORM_VALIDATE_FAILED => HttpResponse::HTTP_BAD_REQUEST,
        self::TOKEN_NOT_PROVIDED => HttpResponse::HTTP_BAD_REQUEST,
        self::PERMISSION_DENIED => HttpResponse::HTTP_FORBIDDEN
    ];

    public static function getStatusMessage($code)
    {
    	return self::$statusTexts[$code];
    }

    public static function getHttpCode($code)
    {
        return self::$httpCode[$code];
    } 

    public static function respondWithError($code,$message = null)
    { 
        return response()->json([
            'status' => 'error',
            'errors' => [
                'message' => $message ?: self::getStatusMessage($code),
                'code' => $code
            ]
        ], self::getHttpCode($code));
    }

}
