<?php

namespace App\Http\Middleware;

use App\Laracart\Helper\ResponseCode;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class JWT extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if (! $token = $this->auth->setRequest($request)->getToken()) {
            return ResponseCode::respondWithError(ResponseCode::TOKEN_NOT_PROVIDED); 
        }

        try {
            
            $user = $this->auth->authenticate($token);

        } catch (TokenExpiredException $e) {  
           
            return ResponseCode::respondWithError(ResponseCode::TOKEN_EXPIRED);
        
        } catch (JWTException $e) {
       
            return ResponseCode::respondWithError(ResponseCode::TOKEN_INVALID); 
       
        }

        if (! $user) {
            return ResponseCode::respondWithError(ResponseCode::USER_NOT_FOUND); 
        }

        $this->events->fire('tymon.jwt.valid', $user); 

        return $next($request);
        
    }
}
