<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Laracart\Documentation\Entities\Api;
use App\Laracart\Product\Entities\Phone;
use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    public function index()
    {
    	return Api::all()->groupBy('group_name');
    }
    public function show($id)
    {
    	return Api::find($id);
    }
}
