<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests;
use App\Http\Requests\Product\PhoneCreateRequest;
use App\Http\Requests\Product\PhoneUpdateRequest;
use App\Laracart\Product\Entities\Phone;
use App\Laracart\Product\Repositories\PhoneRepository;
use Illuminate\Http\Request;

class PhoneController extends ApiController
{
    
    protected $phoneRepository;

	function __construct(PhoneRepository $phoneRepository)
	{
		$this->phoneRepository = $phoneRepository;
        $this->middleware(['jwt']);
	}

    public function index()
    {
        $this->authorize('view',Phone::class);
        $paginator = $this->phoneRepository->getBasicDetailsLists();  
        return $this->respondWithPagination($paginator,
            collect($paginator->toArray()['data'])->keyBy('_id')
        ); 
    } 

    public function edit($id)
    {
        $this->authorize('view',Phone::class);
        $data = $this->phoneRepository->getSinglePhone($id); 
        return $this->respondOK($data);
    }

    public function store(PhoneCreateRequest $request)
    {
        $data = $request->all();
    	$phone = $this->phoneRepository->create($data)->attributesToArray(); 
        unset($phone['created_at'],$phone['updated_at']); 
    	return $this->respondOK($phone);
    }

    public function update(PhoneUpdateRequest $request,$id)
    {
        $data = array_merge(['id' => $id],$request->all());
        $this->phoneRepository->update($data); 
        $phone = $this->phoneRepository->find($id)->attributesToArray();
        unset($phone['created_at'],$phone['updated_at']);
        return $this->respondOK($phone);
    }

}
