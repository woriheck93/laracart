<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    protected $statusCode = 200; 

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function responseNotFound($message = 'Not found!')
    { 
        return $this->setStatusCode(HttpResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    public function respondOK($data = null, $headers = [])
    {
        $data = ['data' => $data];
        $data['status'] = 'OK';   
        return Response::json($data, $this->getStatusCode(), $headers);  
    }

    public function respondWithPagination(LengthAwarePaginator $paginate, $data, $headers = [])
    {  
        $data = ['data' => $data];
        $data['status'] = 'OK';   
        $data = array_merge($data, [
                    'paginator' => [ 
                        'total'         => $paginate->total(),
                        'per_page'      => $paginate->perPage(),
                        'current_page'  => $paginate->currentPage(),
                        'last_page'     => $paginate->lastPage(),
                        'next_page_url' => $paginate->nextPageUrl(),
                        'prev_page_url' => $paginate->previousPageUrl(),
                        'from'          => $paginate->firstItem(),
                        'to'            => $paginate->lastItem(),
                    ]
                ]);
        return Response::json($data, $this->getStatusCode(), $headers);  
    }

    private function respondWithError($message)
    {
        return $this->respond([
            'status' => 'error',
            'errors' => [
                'message' => $message,
                'code' => $this->getStatusCode()
            ]
        ]);
    }
}
