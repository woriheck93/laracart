<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\User\StoreCreateRequest;
use App\Http\Requests\User\StoreEditRequest;
use App\Http\Requests\User\StoreIndexRequest;
use App\Http\Requests\User\StoreUpdateRequest;
use App\Laracart\Transformer\StoreTransformer;
use App\Laracart\User\Entities\Store;
use App\Laracart\User\Repositories\StoreDetailRepository;
use App\Laracart\User\Repositories\StoreRepository;


class StoreController extends ApiController
{

    protected $storeRepository;
    protected $storeTransformer;
    protected $storeDetailRepository;

    public function __construct(StoreRepository $storeRepository,StoreDetailRepository $storeDetailRepository,StoreTransformer $storeTransformer)
    {
        $this->storeRepository = $storeRepository;
        $this->storeTransformer = $storeTransformer;
        $this->storeDetailRepository = $storeDetailRepository;
        $this->middleware(['jwt']);
    }

    /**
     * This is a store paginate list
     * 
     * @return json
     */
    public function index(StoreIndexRequest $storeRequest)
    { 
        $paginator = $this->storeRepository->getStoresDetailLists();
        return $this->respondWithPagination($paginator,
            $this->storeTransformer->transformCollectionWithKey($paginator->all())
        ); 
    }

    /**
     *
     *  This is a single store infomation
     *  @return json
     */
    public function edit(StoreEditRequest $storeRequest, Store $store)
    {
        $data = $this->storeRepository->getStoreDetail($store->id);  
        return $this->respondOK(
            $this->storeTransformer->show($data)
        ); 
    }


    /**
     * This is a registration for a store.
     * 
     * @param App\Http\Requests\User\StoreCreateRequest 
     * @return json
     */
    public function store(StoreCreateRequest $request)
    {
        $info = $request->only('email','password','password_confirmation');
        $details = $request->only('name','phone','address','account','balance'); 
        $store = $this->storeRepository->create($info); 
        $storeDetails = $this->storeDetailRepository->create(array_merge(['user_id' => $store->id],$details))->toArray();  
        return $this->respondOK(
            $this->storeTransformer->store( array_merge($store->toArray(),$storeDetails) )
        ); 
    }

    /**
     * This is update infomation details of a store user.
     *
     * @param App\Http\Requests\User\StoreUpdateRequest
     * @param integer $id
     * @return json
     */
    public function update(StoreUpdateRequest $request, Store $store)
    {        
        $details = $request->only('name','phone','address','account','balance');   
        $this->storeDetailRepository->updateByUserId( array_merge(['user_id' => $store->id],$details) );  
        return $this->respondOK(
            $this->storeTransformer->update($this->storeRepository->getStoreDetail($store->id)['details'])
        ); 
    } 
}
