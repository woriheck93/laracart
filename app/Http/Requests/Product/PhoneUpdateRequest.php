<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Gate;

class PhoneUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update', \App\Laracart\Product\Entities\Phone::class); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'base_price' => 'regex:/^\d*(\.\d{2})?$/',
            'color' => 'Array',
            'size' => 'max:255',
            'weight' => 'max:255',
            'memory' => 'max:255',
            'camera' => 'max:255',
            'capacity' => 'Array',
            'display' => 'max:255',
            'battery' => 'max:255',
            'operating_system' => 'max:255',
            'cpu' => 'max:255',
            'input_output_port' => 'max:255',
            'wireless_technology' => 'max:255',
            'remark' => 'max:255',
        ];
    }
}
