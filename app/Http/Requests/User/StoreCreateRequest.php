<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Gate;

class StoreCreateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    { 
        return Gate::allows('create', \App\Laracart\User\Entities\Store::class); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone'=> 'max:255',
            'address' => 'max:255',
            'account' => 'max:255', 
            'balance' => 'required|regex:/^\d*(\.\d{2})?$/',
        ];
    } 
}
