<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Gate;

class StoreUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $store = $this->route('store');
        return Gate::allows('update', $store); 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => 'required|max:255', 
            'phone'=> 'max:255',
            'address' => 'max:255',
            'account' => 'max:255', 
            'balance' => 'required|regex:/^\d*(\.\d{2})?$/',
        ];
    }
}
