<?php

namespace App\Http\Requests;

use App\Laracart\Helper\ResponseCode;
use Illuminate\Foundation\Http\FormRequest; 


class ApiRequest extends FormRequest
{
	public function response(array $errors)
    { 
		return ResponseCode::respondWithError(ResponseCode::FORM_VALIDATE_FAILED,$errors);
    }

    public function forbiddenResponse()
    { 
        return ResponseCode::respondWithError(ResponseCode::INVALID_ROLE);
    }
}