<?php

use App\Laracart\User\Entities\Admin;
use App\Laracart\User\Entities\Role;
use App\Laracart\User\Entities\Store;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        putenv('DB_CONNECTION=sqlite_testing');
       
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed',['--class'=>'RolesTableSeeder']);
    }

    protected $user;
    protected $token;
    protected $server;

    public function createAdmin()
    {  
        $this->user = factory(Admin::class)->create();
        $this->user->attachRole(Role::name('admin'));
        $this->token = JWTAuth::fromUser($this->user);
        $this->server = ['HTTP_Authorization' => 'Bearer ' . $this->token];
    }

    public function createStore()
    {  
        $this->user = factory(Store::class)->create();
        $this->user->attachRole(Role::name('store'));
        $this->token = JWTAuth::fromUser($this->user);
        $this->server = ['HTTP_Authorization' => 'Bearer ' . $this->token];
    }

}
