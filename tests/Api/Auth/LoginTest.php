<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * URL : /api/login
     * Method : POST
     * Description: This is a login api test
     * 
     */
    public function testLogin()
    {
        /*----------  Admin login  ----------*/
        $this->createAdmin();
    	$parameters = ['email' => $this->user->email, 'password' => 'secret']; 
        $this->post('/api/login', $parameters);  
        $this->seeJsonStructure([
            'data' => ['auth_token'],
            'status',
        ]); 
        Auth::logout();


        /*----------  Store login  ----------*/
        $this->createStore();
        $parameters = ['email' => $this->user->email, 'password' => 'secret']; 
        $this->post('/api/login', $parameters);   
        $this->seeJsonStructure([
            'data' => ['auth_token'],
            'status',
        ]); 
        Auth::logout();
    }
}
