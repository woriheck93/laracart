<?php

use App\Laracart\User\Entities\Store;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class StorePolicyTest extends TestCase
{
	use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
    	$token = JWTAuth::attempt(['email' => 'store@gmail.com','password'=>'secret']);
        $response = $this->call('GET', '/api/store', [], [], [], ['HTTP_Authorization'=>'Bearer '.$token], null);    
        $this->assertTrue(true);
    }

    public function testEdit()
    {
        $token = JWTAuth::attempt(['email' => 'store2@gmail.com','password'=>'secret']);
        $response = $this->call('GET', '/api/store/2/edit', [], [], [], ['HTTP_Authorization'=>'Bearer '.$token], null);    
        $this->assertTrue(true);
    }

    public function testUpdate()
    {
        $token = JWTAuth::attempt(['email' => 'store@gmail.com','password'=>'secret']);
        $response = $this->call('GET', '/api/store/2/edit', [], [], [], ['HTTP_Authorization'=>'Bearer '.$token], null);  
        $this->assertTrue(true);
    }

}
