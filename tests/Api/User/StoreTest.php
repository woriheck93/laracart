<?php

use App\Laracart\User\Entities\Store;
use App\Laracart\User\Entities\StoreDetail;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth; 

class StoreTest extends TestCase
{
	use DatabaseTransactions;

   /**
     * URL : /api/store
     * Method: GET
     * Description: This is a store index test.
     * Role : admin
     */

    public function testIndex()
    {
        $storeCollection = factory(StoreDetail::class,10)->create();
        $this->createAdmin();
        $this->get('/api/store', $this->server);
        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'email',
                    'phone',
                    'address',
                    'account',
                    'balance'
                ]
            ],
            'paginator' => [
                'total',
                'per_page',
                'current_page',
                'last_page',
                'next_page_url',
                'prev_page_url',
                'from'
            ],
            'status'
        ]); 
    }

    /**
     * URL : /api/store
     * Method: GET
     * Description: This is a store edit test.
     * Role : admin, ownder
     */

    public function testEdit()
    {
        $store = factory(StoreDetail::class)->create(); 
        $this->createAdmin();
        $this->get('/api/store/' . $store->id . '/edit',$this->server); 
        $this->seeJsonStructure([
            'data' => [ 
                'id',
                'name',
                'email',
                'phone',
                'address',
                'account',
                'balance' 
            ], 
            'status'
        ]); 
    }

    /**
     * URL : /api/store/create
     * Method: POST
     * Description: This is a store create test.
     * Role : admin
     */

    public function testCreate()
    {
        $store = factory(Store::class,'parameter')->make()->toArray(); 
        $storeDetail = factory(StoreDetail::class,'parameter')->make()->toArray(); 
        $password = ['password' => 'secret', 'password_confirmation' => 'secret'];
        $parameters = array_merge($store,$storeDetail,$password);

        $this->createAdmin();
        $this->post('/api/store/create', $parameters, $this->server);
        $this->seeInDatabase('users',$store);
        $this->seeInDatabase('store_details',$storeDetail);
        $this->seeJsonStructure([
            'status',
            'data' => [
                'id',
                'name',
                'email',
                'phone',
                'address',
                'account',
                'balance'            
            ]
        ]); 
    }

    /**
     * URL : /api/store/{id}/update
     * Method: POST
     * Description: This is a store update test.
     * Role : admin, owner
     */

    public function testUpdate()
    { 
        $storeId = factory(StoreDetail::class)->create()->user_id; 
        $parameters = factory(StoreDetail::class,'parameter')->make()->toArray(); 
        
        $this->createAdmin();
        $this->put('/api/store/' . $storeId , $parameters, $this->server);
        $this->seeInDatabase('store_details',$parameters); 
        $this->seeJsonEquals([
            'status' => 'OK',
            'data' => [
                'name'      =>  $parameters['name'],
                'phone'     =>  $parameters['phone'],
                'address'   =>  $parameters['address'],
                'account'   =>  $parameters['account'], 
                'balance'   =>  json_encode($parameters['balance'])
            ]
        ]);
    } 
    
}
