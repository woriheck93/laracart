<?php

use App\Laracart\Product\Entities\Phone;
use App\Laracart\User\Entities\Admin;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;

class PhoneTest extends TestCase
{
    use DatabaseTransactions; 

    /**
     * URL : /api/product/phone
     * Method: GET
     * Description: This is a phone index test
     * Role : admin
     */

    public function testIndex()
    {
        $this->createAdmin();
        $response = $this->get('/api/product/phone',$this->server); 
        $this->seeJsonStructure([
            'data' => [
                '*' => [
                     '_id','name','base_price','color','size','weight','memory','capacity'
                ]
            ],
            'paginator' => [
                'total',
                'per_page',
                'current_page',
                'last_page',
                'next_page_url',
                'prev_page_url',
                'from'
            ],
            'status'
        ]);
    }

    /**
     * URL: /api/product/phone/{id}/edit 
     * Method: GET
     * Description: This is a phone edit test.
     * Role: admin
     */
    
    public function testEdit()
    { 
        $phone = factory(Phone::class)->create();
        $this->createAdmin();
        $this->get('/api/product/phone/' . $phone->id . '/edit',$this->server); 
        $this->seeJsonStructure([
            'data' =>[
                '_id', 
                'name', 
                'base_price', 
                'color' =>  [],
                'size',  
                'weight', 
                'memory', 
                'camera', 
                'capacity' => [],
                'display',
                'battery', 
                'operating_system',  
                'cpu',
                'input_output_port',
                'wireless_technology',  
                'remark',
                'updated_at',  
                'created_at',  
            ], 
            'status'
        ]);
    }

    /**
     * URL: /api/product/phone/create
     * Method: POST
     * Description: This is a phone create test.
     * Role: admin
     */

    public function testCreate()
    { 
        $phone = factory(Phone::class)->make()->toArray();
        $parameters = array_merge([ 'image' => $this->getTestImage() ], $phone ); 
        $this->createAdmin();
        $this->post('/api/product/phone/create',$parameters,$this->server); 
        $this->seeJsonStructure([
            'data' =>[
                '_id', 
                'name', 
                'base_price', 
                'color' =>  [],
                'size',  
                'weight', 
                'memory', 
                'camera', 
                'capacity' => [],
                'display',
                'battery', 
                'operating_system',  
                'cpu',
                'input_output_port',
                'wireless_technology',  
                'remark'
            ], 
            'status'
        ]);
    }

    /**
     * URL: /api/product/phone/create
     * Method: POST
     * Description: This is a phone update test.
     * Role: admin
     */

    public function testUpdate()
    { 
        $phoneId = factory(Phone::class)->create()->_id;
        $parameters = factory(Phone::class)->make()->toArray();
        $this->createAdmin();
        $response = $this->put('/api/product/phone/' . $phoneId, $parameters, $this->server);
        $parameters['_id'] = $phoneId;
        $this->seeJsonEquals(array_merge(['data' => $parameters],['status'=>'OK']));
    }

    /** 
     * Description: To generate a testing image.
     * 
     * @return Illuminate\Http\UploadedFile;
     */

    private function getTestImage()
    {
        $stub = __DIR__.'/images/test.jpg';
        $name = str_random(8).'.png';
        $path = sys_get_temp_dir().'/'.$name;
        copy($stub, $path);
        return new UploadedFile($path, $name, filesize($path), 'image/png', null, true); 
    }
}
