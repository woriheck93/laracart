
new Vue({
	el: '#app',
	data:{ 
	    api : '', 
	    document: '',
	    loading: true
	},
	beforeCreate(){
		this.$http.get('/api/document').then((response) => {
			this.document = response.body;
			this.api = this.document['Authenticate Api'][0]  
			this.loading = false
		}, (response) => {
		// error callback
		});
	},
	methods: {
    	getApi: function (groupName,index) 
    	{ 
			this.api.visible = false;
			self = this;
			setTimeout(function(){   
				self.api = self.document[groupName][index] 
				self.api.visible = true;
			}, 100); 
	    }
  	},
  	filters: {
	    capitalize: function (value) {
	      if (!value) return ''
	      value = value.toString()
	      return value.charAt(0).toUpperCase() + value.slice(1)
	    }
	  }
}); 