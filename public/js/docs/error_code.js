var error_code = {  
	'1xx' : [
    	{
    		'code': 100, 
    		'name' : 'USER NOT FOUND', 
    		'description': 'Undefined user.'
    	},
    	{
    		'code': 101, 
    		'name' : 'INVALID LOGIN', 
    		'description': 'Login Failed.'
    	},
    	{
    		'code': 102, 
    		'name' : 'INVALID ROLE', 
    		'description': 'Permission denied, your role are not authorize to do this action.'
    	},
    	{
    		'code': 103, 
    		'name' : 'TOKEN EXPIRED', 
    		'description': 'Token expired, relogin again.'
    	},
    	{
    		'code': 104, 
    		'name' : 'TOKEN INVALID', 
    		'description': 'Token invalid, request failed.'
    	},
        {
            'code': 105, 
            'name' : 'PERMISSION DENIED', 
            'description': 'Permission denied, you are not authorize to do this action.'
        },
        {
            'code': 106, 
            'name' : 'TOKEN NOT PROVIDED', 
            'description': 'Request failed, token not provided.'
        },
	],

    '3xx' : [
        {
            'code': 300, 
            'name' : 'FORM_VALIDATE_FAILED', 
            'description': 'Input error ! ',
        },
    ],
}
	    