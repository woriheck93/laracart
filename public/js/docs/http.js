new Vue({
	el: '#app',
	data:{  
		http_status: http_status,
	    code: {
	    	'1xx' : 'Information',
	    	'2xx' : 'Successful',
	    	'3xx' : 'Redirection',
	    	'4xx' : 'Client Error',
	    	'5xx' : 'Server Error',
	    },
	    color: [
	    	'uk-text-primary',
	    	'uk-text-success',
	    	'uk-text-warning',
	    	'uk-text-danger',
	    	'uk-text-danger'
	    ],
	    highlight: 0
	},
	mounted(){
		this.highlight = window.location.hash.substr(1);
		console.log(this.highlight);
	}
}); 