new Vue({
	el: '#app',
	data:{  
		error_code: error_code,
	    code: {
	    	'1xx' : 'Error User',
	    	'2xx' : 'Successful',
	    	'3xx' : 'Validation Failed',
	    	'4xx' : 'Client Error',
	    	'5xx' : 'Server Error',
	    },
	    color: [
	    	'uk-text-primary',
	    	'uk-text-success',
	    	'uk-text-warning',
	    	'uk-text-danger',
	    	'uk-text-danger'
	    ],
	    highlight: 0
	},
	mounted(){
		this.highlight = window.location.hash.substr(1);
		console.log(this.highlight);
	}
}); 