var api = [

    /*=============================================
    =                    Login                    =
    =============================================*/

	{
    	'visible' : true,
    	'title' : 'Login',
    	'version' : '1.0.0',
    	'method' : 'POST',
    	'path' : '/api/login',
    	'summary' : 'This is an authenticate layer for validate a user.',
    	'description' : 'By default, the user will not be able to login for one minute if they fail to provide the correct credentials after several attempts.',
    	'parameters' : [
    		{ 
    			'name' : 'email' , 
    			'located_in' : 'Input', 
    			'description' : 'An account email address' , 
    			'required' : true , 
    			'schema' : 'email' 
    		},
    		{ 
    			'name' : 'password', 
    			'located_in' : 'Input', 
    			'description' : 'An account password', 
    			'required' : true , 
    			'schema' : 'string (255)' 
    		} 
    	],
    	'responses' : [
    		{ 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'auth_token', 
                'located_in' : 'body', 
                'description' : 'Token that represent the current user.', 
                'required' : true , 
                'schema' : 'JWT string' 
            } 
    	],    
        'sample' : [
            { 
                'email': 'hschaefer@example.com',
                'password': 'secret'
            },
            {
                'status'    : 'OK',
                'auth_token' : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdCIsImlhdCI6MTQ3NTkyOTI5MSwiZXhwIjoxNDc1OTMyODkxLCJuYmYiOjE0NzU5MjkyOTEsImp0aSI6IjUwMWIyM2EwZTU0YmQzNjQxYjRkNWNkYWM5OTQ2MzY2In0.f3Cz9OhudAcodc8HW8IvNAhsC7YQoctrmq_38iJyeGM' 
            }
        ]
    },

    /*=====  End of Section login block  ======*/

     /*==================================================
    =            Section Store list block              =
    ==================================================*/

    {
        'visible' : true,
        'title' : 'Store List',
        'version' : '1.0.0',
        'method' : 'GET',
        'path' : '/api/store',
        'summary' : 'This is a list of store user.',
        'description' : 'Currently, only admin had the privilege to view a store information.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            }
        ],
        'responses' : [
            { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'name' , 
                'located_in' : 'body', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'email' , 
                'located_in' : 'body', 
                'description' : 'An account email address' , 
                'required' : true , 
                'schema' : 'email' 
            },
            { 
                'name' : 'phone', 
                'located_in' : 'body', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'body', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'body', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'body', 
                'description' : 'Account balance', 
                'required' : true  , 
                'schema' : 'Double' 
            },
            { 
                'name' : 'paginator', 
                'located_in' : 'body', 
                'description' : 'List paginator', 
                'required' : true  , 
                'schema' : 'JSON' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {  
            },
            {
                'status' : 'OK',
                'data' : [{
                    'id': 2, 
                    'name': 'Erik Chow', 
                    'email': 'erikchow1993@gmail.com',
                    'phone': '0911158894',
                    'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                    'account' : '9322091122003943',
                    'balance' : 12000
                }],
                'paginator' : {
                    'total' : 1,
                    'per_page' : 15,
                    'current_page': 1,
                    'last_page': 1,
                    'next_page_url': null,
                    'prev_page_url': null,
                    'from': 1,
                    'to': 1
                }
            }
        ]
    },
    
    /*=====  End of Store List block  ======*/


    /*=============================================
    =            Stock Registration              =
    =============================================*/ 

    {
        'visible' : true,
        'title' : 'Store Create',
        'version' : '1.0.0',
        'method' : 'POST',
        'path' : '/api/store/create',
        'summary' : 'This is a store create API.',
        'description' : 'Currently, only admin had the privilege to register a store.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            },
            { 
                'name' : 'name' , 
                'located_in' : 'Input', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'email' , 
                'located_in' : 'Input', 
                'description' : 'An account email address' , 
                'required' : true , 
                'schema' : 'email' 
            },
            { 
                'name' : 'password', 
                'located_in' : 'Input', 
                'description' : 'An account password', 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'password_confirmation', 
                'located_in' : 'Input', 
                'description' : 'An account password confirmation', 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'phone', 
                'located_in' : 'Input', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'Input', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'Input', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'Input', 
                'description' : 'Account balance', 
                'required' : true  , 
                'schema' : 'Double' 
            }
        ],
        'responses' : [
            { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'name' , 
                'located_in' : 'body', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'email' , 
                'located_in' : 'body', 
                'description' : 'An account email address' , 
                'required' : true , 
                'schema' : 'email' 
            }, 
            { 
                'name' : 'phone', 
                'located_in' : 'body', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'body', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'body', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'body', 
                'description' : 'Account balance', 
                'required' : true  , 
                'schema' : 'Double' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            { 
                'name': 'Erik Chow',
                'email': 'erikchow1993@gmail.com',
                'password': 'secret',
                'password_confirmation': 'secret',
                'phone': '0911158894',
                'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                'account' : '9322091122003943',
                'balance' : 12000
            },
            {
                'status' : 'OK',
                'data' : {
                    'id' : 8,
                    'name': 'Erik Chow',
                    'email': 'erikchow1993@gmail.com',
                    'phone': '0911158894',
                    'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                    'account' : '9322091122003943',
                    'balance' : 12000
                }
            }
        ]
    },

    /*=====  End of Section Store Registration  ======*/

    /*==================================================
    =            Section Store Read block              =
    ==================================================*/

    {
        'visible' : true,
        'title' : 'Store Edit',
        'version' : '1.0.0',
        'method' : 'GET',
        'path' : '/api/store/{id}/edit',
        'summary' : 'This is a single store user.',
        'description' : 'Currently, only admin and the store owner had the privilege to view a store information.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            }
        ],
        'responses' : [
           { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'name' , 
                'located_in' : 'body', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'email' , 
                'located_in' : 'body', 
                'description' : 'An account email address' , 
                'required' : true , 
                'schema' : 'email' 
            }, 
            { 
                'name' : 'phone', 
                'located_in' : 'body', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'body', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'body', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'body', 
                'description' : 'Account balance', 
                'required' : true  , 
                'schema' : 'Double' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {  
            },
            {
                'status' : 'OK',
                'data' : {
                    'id': 2, 
                    'name': 'Erik Chow', 
                    'email': 'erikchow1993@gmail.com',
                    'phone': '0911158894',
                    'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                    'account' : '9322091122003943',
                    'balance' : 12000
                }
            }
        ]
    },
    
    /*=====  End of Store Read block  ======*/

    /*==================================================
    =            Section Store Update block            =
    ==================================================*/

    {
        'visible' : true,
        'title' : 'Store Update',
        'version' : '1.0.0',
        'method' : 'POST',
        'path' : '/api/store/{id}',
        'summary' : 'This is update infomation details of a store user.',
        'description' : 'Currently, only admin and the store owner had the privilege to update a store information.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            },
            { 
                'name' : '_method' , 
                'located_in' : 'Input', 
                'description' : 'PUT', 
                'required' : true, 
                'schema' : 'string' 
            },
            { 
                'name' : 'name' , 
                'located_in' : 'Input', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'phone', 
                'located_in' : 'Input', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'Input', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'Input', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'Input', 
                'description' : 'Account balance', 
                'required' : false , 
                'schema' : 'Double' 
            }
        ],
        'responses' : [
           { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            }, 
            { 
                'name' : 'name' , 
                'located_in' : 'body', 
                'description' : 'An account name' , 
                'required' : true , 
                'schema' : 'string (255)' 
            }, 
            { 
                'name' : 'phone', 
                'located_in' : 'body', 
                'description' : 'Store\'s phone', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'address', 
                'located_in' : 'body', 
                'description' : 'Store\'s address', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'account', 
                'located_in' : 'body', 
                'description' : 'A bank account', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'balance', 
                'located_in' : 'body', 
                'description' : 'Account balance', 
                'required' : true  , 
                'schema' : 'Double' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            { 
                '_method': 'PUT',
                'name': 'Erik Chow', 
                'phone': '0911158894',
                'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                'account' : '9322091122003943',
                'balance' : 12000
            },
            {
                'status' : 'OK',
                'data' : {
                    'name': 'Erik Chow', 
                    'phone': '0911158894',
                    'address' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ipsa dignissimos quos!',
                    'account' : '9322091122003943',
                    'balance' : 12000
                }
            }
        ]
    },
    
    /*=====  End of Store Update block  ======*/
    
    /*==========================================
    =            Section Phone List            =
    ==========================================*/
    
     {
        'visible' : true,
        'title' : 'Phone List',
        'version' : '1.0.0',
        'method' : 'GET',
        'path' : '/api/product/phone',
        'summary' : 'This is a phone catalog list API',
        'description' : 'Currently, only admin had the privilege to view the phone product list.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            } 
        ],
        'responses' : [
            { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : '_id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'ObjectId' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'paginator', 
                'located_in' : 'body', 
                'description' : 'List paginator', 
                'required' : true  , 
                'schema' : 'JSON' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {
            },
            {
                'status' : 'OK',
                'data': [{
                    '_id' : '580b1734fd877469bd4df211',
                    'name' : 'Iphone 7',
                    'base_price' : 599,
                    'color' : ['gold','silver','dark'],
                    'size' : '5.44-inch',
                    'weight' : '155 g',
                    'memory' : 'RAM 3GB / 4GB LPDDR3',
                    'capacity' : [ '32GB', '64GB'],
               }],
               'paginator' : {
                    'total' : 1,
                    'per_page' : 15,
                    'current_page': 1,
                    'last_page': 1,
                    'next_page_url': null,
                    'prev_page_url': null,
                    'from': 1,
                    'to': 1
                }
            }
        ]
    },

    
    
    /*=====  End of Section Phone List  ======*/
    

    /*==================================================
    =            Section Phone create block            =
    ==================================================*/
    {
        'visible' : true,
        'title' : 'Phone Create',
        'version' : '1.0.0',
        'method' : 'POST',
        'path' : '/api/product/phone/create',
        'summary' : 'This is a phone catalog create API',
        'description' : 'Currently, only admin had the privilege to create a phone product.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'camera', 
                'located_in' : 'Input', 
                'description' : 'Phone front & end camera', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'display', 
                'located_in' : 'Input', 
                'description' : 'Phone screen display', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'battery', 
                'located_in' : 'Input', 
                'description' : 'Phone battery', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'operating_system', 
                'located_in' : 'Input', 
                'description' : 'Phone OS', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'cpu', 
                'located_in' : 'Input', 
                'description' : 'CPU', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'input_output_port', 
                'located_in' : 'Input', 
                'description' : 'I/O port', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'wireless_technology', 
                'located_in' : 'Input', 
                'description' : 'Wireless support', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'remark', 
                'located_in' : 'Input', 
                'description' : 'Reservation column', 
                'required' : false , 
                'schema' : 'string (255)' 
            }
        ],
        'responses' : [
             { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : '_id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'ObjectId' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'camera', 
                'located_in' : 'Input', 
                'description' : 'Phone front & end camera', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'display', 
                'located_in' : 'Input', 
                'description' : 'Phone screen display', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'battery', 
                'located_in' : 'Input', 
                'description' : 'Phone battery', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'operating_system', 
                'located_in' : 'Input', 
                'description' : 'Phone OS', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'cpu', 
                'located_in' : 'Input', 
                'description' : 'CPU', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'input_output_port', 
                'located_in' : 'Input', 
                'description' : 'I/O port', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'wireless_technology', 
                'located_in' : 'Input', 
                'description' : 'Wireless support', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'remark', 
                'located_in' : 'Input', 
                'description' : 'Reservation column', 
                'required' : false , 
                'schema' : 'string (255)' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {
                'name' : 'Iphone 7',
                'base_price' : 599,
                'color' : ['gold','silver','dark'],
                'size' : '5.44-inch',
                'weight' : '155 g',
                'memory' : 'RAM 3GB / 4GB LPDDR3',
                'camera' : '8MP Camera , f/2.0 aperture',
                'capacity' : [ '32GB', '64GB'],
                'display' : 'Retina display ',
                'battery' : '3000mAh (non-removable)',
                'operating_system' : 'Android™ 6.0 with brand-new ASUS ZenUI 3.0',
                'cpu' : '64-bit Qualcomm® Octa-Core ProcessorSnapdragon™ 625 @2.0Ghz',
                'input_output_port' : '3.5mm audio jack(1 Head phone / Mic-in)',
                'wireless_technology' : '802.11b/g/n/ac , Bluetooth V 4.2 , Wi-Fi direct',
                'remark' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non exercitationem explicabo, asperiores laudantium impedit! Tempore iure, minus quae consectetur sapiente?',
            },
            {
                'status' : 'OK',
                'data': {
                    '_id' : '580c63a2f57e79312a2f42e1',
                    'name' : 'Iphone 7',
                    'base_price' : 599,
                    'color' : ['gold','silver','dark'],
                    'size' : '5.44-inch',
                    'weight' : '155 g',
                    'memory' : 'RAM 3GB / 4GB LPDDR3',
                    'camera' : '8MP Camera , f/2.0 aperture',
                    'capacity' : [ '32GB', '64GB'],
                    'display' : 'Retina display ',
                    'battery' : '3000mAh (non-removable)',
                    'operating_system' : 'Android™ 6.0 with brand-new ASUS ZenUI 3.0',
                    'cpu' : '64-bit Qualcomm® Octa-Core ProcessorSnapdragon™ 625 @2.0Ghz',
                    'input_output_port' : '3.5mm audio jack(1 Head phone / Mic-in)',
                    'wireless_technology' : '802.11b/g/n/ac , Bluetooth V 4.2 , Wi-Fi direct',
                    'remark' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non exercitationem explicabo, asperiores laudantium impedit! Tempore iure, minus quae consectetur sapiente?',
                }
            }
        ]
    },
    
    
    /*=====  End of Phone Create block  ======*/


     /*==================================================
    =            Section Phone create block            =
    ==================================================*/
    {
        'visible' : true,
        'title' : 'Phone Edit',
        'version' : '1.0.0',
        'method' : 'GET',
        'path' : '/api/product/phone/{_id}/edit',
        'summary' : 'This is a phone catalog edit API',
        'description' : 'Currently, only admin had the privilege to edit a phone product.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            },
        ],
        'responses' : [
            { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : '_id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'ObjectId' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'camera', 
                'located_in' : 'Input', 
                'description' : 'Phone front & end camera', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'display', 
                'located_in' : 'Input', 
                'description' : 'Phone screen display', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'battery', 
                'located_in' : 'Input', 
                'description' : 'Phone battery', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'operating_system', 
                'located_in' : 'Input', 
                'description' : 'Phone OS', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'cpu', 
                'located_in' : 'Input', 
                'description' : 'CPU', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'input_output_port', 
                'located_in' : 'Input', 
                'description' : 'I/O port', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'wireless_technology', 
                'located_in' : 'Input', 
                'description' : 'Wireless support', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'remark', 
                'located_in' : 'Input', 
                'description' : 'Reservation column', 
                'required' : false , 
                'schema' : 'string (255)' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {},
            {
                'status' : 'OK',
                'data': {
                    '_id' : '580b1734fd877469bd4df211',
                    'name' : 'Iphone 7',
                    'base_price' : 599,
                    'color' : [
                        'gold',
                        'silver',
                        'dark'
                    ],
                    'size' : '5.44-inch',
                    'weight' : '155 g',
                    'memory' : 'RAM 3GB / 4GB LPDDR3',
                    'camera' : '8MP Camera , f/2.0 aperture',
                    'capacity' : [
                        '32GB',
                        '64GB',
                    ],
                    'display' : 'Retina display ',
                    'battery' : '3000mAh (non-removable)',
                    'operating_system' : 'Android™ 6.0 with brand-new ASUS ZenUI 3.0',
                    'cpu' : '64-bit Qualcomm® Octa-Core ProcessorSnapdragon™ 625 @2.0Ghz',
                    'input_output_port' : '3.5mm audio jack(1 Head phone / Mic-in)',
                    'wireless_technology' : '802.11b/g/n/ac , Bluetooth V 4.2 , Wi-Fi direct',
                    'remark' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non exercitationem explicabo, asperiores laudantium impedit! Tempore iure, minus quae consectetur sapiente?',
                    'updated_at' : '2016-10-22 09:49:35',
                    'created_at' : '2016-10-22 07:37:23',
                }
            }
        ]
    },
    
    
    /*=====  End of Phone Edit block  ======*/

    /*==================================================
    =            Section Phone Update block            =
    ==================================================*/
    {
        'visible' : true,
        'title' : 'Phone Update',
        'version' : '1.0.0',
        'method' : 'POST',
        'path' : '/api/product/phone/{id}',
        'summary' : 'This is a phone catalog update API',
        'description' : 'Currently, only admin had the privilege to update a phone product.',
        'parameters' : [ 
            { 
                'name' : 'token' , 
                'located_in' : 'header', 
                'description' : 'An admin/store role JWT token' , 
                'required' : true , 
                'schema' : 'JWT String' 
            },
            { 
                'name' : '_method' , 
                'located_in' : 'Input', 
                'description' : 'PUT request' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'camera', 
                'located_in' : 'Input', 
                'description' : 'Phone front & end camera', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'display', 
                'located_in' : 'Input', 
                'description' : 'Phone screen display', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'battery', 
                'located_in' : 'Input', 
                'description' : 'Phone battery', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'operating_system', 
                'located_in' : 'Input', 
                'description' : 'Phone OS', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'cpu', 
                'located_in' : 'Input', 
                'description' : 'CPU', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'input_output_port', 
                'located_in' : 'Input', 
                'description' : 'I/O port', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'wireless_technology', 
                'located_in' : 'Input', 
                'description' : 'Wireless support', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'remark', 
                'located_in' : 'Input', 
                'description' : 'Reservation column', 
                'required' : false , 
                'schema' : 'string (255)' 
            }
        ],
        'responses' : [
            { 
                'name' : 'status' , 
                'located_in' : 'body', 
                'description' : 'Response Status.' , 
                'required' : true , 
                'schema' : 'string' 
            },
            { 
                'name' : '_id' , 
                'located_in' : 'body', 
                'description' : 'Specific id' , 
                'required' : true , 
                'schema' : 'ObjectId' 
            },
            { 
                'name' : 'name', 
                'located_in' : 'Input', 
                'description' : 'Phone name', 
                'required' : true, 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'base_price', 
                'located_in' : 'Input', 
                'description' : 'Phone base price', 
                'required' : false , 
                'schema' : 'Double (precision 2)' 
            },
            { 
                'name' : 'color', 
                'located_in' : 'Input', 
                'description' : 'Phone color', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'size', 
                'located_in' : 'Input', 
                'description' : 'Phone size', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'weight', 
                'located_in' : 'Input', 
                'description' : 'Phone weight', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'memory', 
                'located_in' : 'Input', 
                'description' : 'Phone memory', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'camera', 
                'located_in' : 'Input', 
                'description' : 'Phone front & end camera', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'capacity', 
                'located_in' : 'Input', 
                'description' : 'Phone internal storage, memory card', 
                'required' : false , 
                'schema' : 'Array' 
            },
            { 
                'name' : 'display', 
                'located_in' : 'Input', 
                'description' : 'Phone screen display', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'battery', 
                'located_in' : 'Input', 
                'description' : 'Phone battery', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'operating_system', 
                'located_in' : 'Input', 
                'description' : 'Phone OS', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'cpu', 
                'located_in' : 'Input', 
                'description' : 'CPU', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'input_output_port', 
                'located_in' : 'Input', 
                'description' : 'I/O port', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'wireless_technology', 
                'located_in' : 'Input', 
                'description' : 'Wireless support', 
                'required' : false , 
                'schema' : 'string (255)' 
            },
            { 
                'name' : 'remark', 
                'located_in' : 'Input', 
                'description' : 'Reservation column', 
                'required' : false , 
                'schema' : 'string (255)' 
            }
        ],
        'header': 'Authorization: Bearer {JWT-token}',
        'sample' : [ 
            {  
                '_method': 'PUT',
                'name' : 'Iphone 7',
                'base_price' : 599,
                'color' : ['gold','silver','dark'],
                'size' : '5.44-inch',
                'weight' : '155 g',
                'memory' : 'RAM 3GB / 4GB LPDDR3',
                'camera' : '8MP Camera , f/2.0 aperture',
                'capacity' : [ '32GB', '64GB'],
                'display' : 'Retina display ',
                'battery' : '3000mAh (non-removable)',
                'operating_system' : 'Android™ 6.0 with brand-new ASUS ZenUI 3.0',
                'cpu' : '64-bit Qualcomm® Octa-Core ProcessorSnapdragon™ 625 @2.0Ghz',
                'input_output_port' : '3.5mm audio jack(1 Head phone / Mic-in)',
                'wireless_technology' : '802.11b/g/n/ac , Bluetooth V 4.2 , Wi-Fi direct',
                'remark' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non exercitationem explicabo, asperiores laudantium impedit! Tempore iure, minus quae consectetur sapiente?',
            },
            {
                'status' : 'OK',
                'data' : {
                    'name' : 'Iphone 7',
                    'base_price' : 599,
                    'color' : ['gold','silver','dark'],
                    'size' : '5.44-inch',
                    'weight' : '155 g',
                    'memory' : 'RAM 3GB / 4GB LPDDR3',
                    'camera' : '8MP Camera , f/2.0 aperture',
                    'capacity' : [ '32GB', '64GB'],
                    'display' : 'Retina display ',
                    'battery' : '3000mAh (non-removable)',
                    'operating_system' : 'Android™ 6.0 with brand-new ASUS ZenUI 3.0',
                    'cpu' : '64-bit Qualcomm® Octa-Core ProcessorSnapdragon™ 625 @2.0Ghz',
                    'input_output_port' : '3.5mm audio jack(1 Head phone / Mic-in)',
                    'wireless_technology' : '802.11b/g/n/ac , Bluetooth V 4.2 , Wi-Fi direct',
                    'remark' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non exercitationem explicabo, asperiores laudantium impedit! Tempore iure, minus quae consectetur sapiente?',
                }
            }
        ]
    },

    /*=====  End of Phone Update block  ======*/

]