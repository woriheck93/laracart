@extends('api.layout')

@section('content')

<div id="app"  class="uk-container uk-container-center">
	<div class="uk-grid">

		<div class="uk-width-1-1">
			<div class="status_code" v-for="(status, key, index) in error_code">
				<h2 class="get-title">
			   		@{{ key }}:
			   		@{{ code[key] }}
			   	</h2>
				<table class="uk-table  uk-table-hover">
						<thead>
			   			<tr>
			   				<th class="collapsing">
			   					Code
			   				</th> 
			   				<th>
			   					Name
			   				</th> 
			   				<th>
			   					Description
			   				</th>
			   			</tr>
						</thead>
						<tbody>
							<tr v-bind:id="data.code" v-for="(data, key, index) in status" 
								v-bind:class="{ 'uk-text-uppercase uk-text-bold uk-text-primary': data.code == highlight }">
				   				<td class="collapsing uk-width-1-10">
				   					@{{ data.code }}
				   				</td>
				   				<td class="uk-width-2-10">
				   					@{{ data.name }}
				   				</td>
				   				<td class="uk-width-7-10">
				   					@{{ data.description }}
				   				</td>
			   				</tr>  
						</tbody>
				</table> 
				<br>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
<script src="/js/docs/error_code.js"></script>	
<script src="/js/docs/error.js"></script>
@endsection