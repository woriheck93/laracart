@extends('api.layout')

@section('content')

	<div id="app" class="uk-container uk-container-center" style="min-height: 100vh;">
		<div class="uk-grid">
		    <div class="tm-sidebar uk-width-medium-1-4 uk-hidden-small uk-row-first" style="border-right: 1px solid #E5E5E5;">
				<ul  v-if="!loading" class="tm-nav uk-nav" data-uk-nav=""> 
					<template v-for="(groups, groupName) in document">
						<li class="uk-nav-header">
							@{{groupName}}
						</li> 
						<li v-for="(item,index) in groups"> 
							<a v-on:click="getApi(groupName,index)">
								@{{ item.title }}
							</a>
						</li>
					</template> 
				</ul>
			</div>

			<div id="article" class="tm-main uk-width-medium-3-4" > 

				<div v-if="loading" align="center">
					<i class="uk-icon-spinner uk-icon-spin uk-icon-large"></i>
				</div>

		    	<article  v-if="!loading" v-show="api.visible" class="uk-article"  v-bind:class="{'fadeInUp animated': api.visible }">
				    <h1 class="uk-article-title">
				    	@{{ api.title }}
				    </h1>
				    <p class="uk-api-meta">
				    	Version @{{ api.version }}
				    </p> 
				   	<span class="ui green basic label">
				   		<i class="uk-icon-download" v-show="api.method === 'GET' ">&nbsp;&nbsp;</i> 
				   		@{{ api.method }}
				   	</span>
					&nbsp;
				   	<span style="font-size:20px;">
						<B>@{{ api.path }}</B>
				   	</span>

				   	<h2 class="get-title">
				   		Permission
				   	</h2>
				   	<p v-for="role in api.role">	
				   		@{{ role | capitalize }}
				   	</p>

				   	<h2 class="get-title">
				   		Summary
				   	</h2>
				   	<p>	
				   		@{{ api.summary }} 
				   	</p>

				   	<h2 class="get-title">
				   		Description
				   	</h2>
					<p>
						@{{ api.description }} 
					</p>
					<h2 class="get-title">
				   		Parameters
				   	</h2>
				   	<p>
				   		<table class="uk-table uk-table-hover">
				   			<thead>
					   			<tr>
					   				<th>
					   					Name
					   				</th>
					   				<th>
					   					Located in
					   				</th>
					   				<th>
					   					Description
					   				</th>
					   				<th class="collapsing">
					   					Required
					   				</th>
					   				<th>
					   					Schema
					   				</th>
					   			</tr>
				   			</thead>
				   			<tbody>
					   			<tr v-for="parameter in api.parameters">
					   				<td>@{{ parameter.name }}</td>
					   				<td>@{{ parameter.located_in }}</td>
					   				<td>@{{ parameter.description }}</td>
					   				<td>@{{ parameter.required }}</td>
					   				<td class="collapsing">@{{ parameter.schema }}</td>
					   			</tr> 
					   		</tbody>
				   		</table>
				   	</p>
				   	<h2 class="get-title">
				   		Responses
				   	</h2>
					<p>
				   		<table class="uk-table uk-table-hover">
				   			<thead>
					   			<tr> 
					   				<th>
					   					Name
					   				</th>
					   				<th>
					   					Located in
					   				</th>
					   				<th>
					   					Description
					   				</th>
					   				<th class="collapsing">
					   					Required
					   				</th>
					   				<th>
					   					Schema
					   				</th>
					   			</tr>
				   			</thead>
				   			<tbody>
				   				<tr v-for="response in api.responses">
					   				<td>@{{ response.name }}</td>
					   				<td>@{{ response.located_in }}</td>
					   				<td>@{{ response.description }}</td>
					   				<td>@{{ response.required }}</td>
					   				<td class="collapsing">@{{ response.schema }}</td>
					   			</tr>  
				   			</tbody>
				   		</table>
				   	</p> 
				    <hr class="uk-article-divider">
			   		<h2 class="get-title">
				   		Sample I/O
				 	</h2>
				    <div class="uk-accordion" data-uk-accordion>
						<h3 class="uk-accordion-title">Input</h3>
						<div class="uk-accordion-content">
							<pre v-show="api.header">@{{api.header}}</pre>
							<pre>@{{ api.sample[0] }}</pre>
						</div>
						<h3 class="uk-accordion-title">Output</h3>
						<div class="uk-accordion-content">
							<pre>@{{ api.sample[1] }}</pre>
						</div> 
					</div>
				</article>
		    </div> 
		</div>
	</div>

@endsection

@section('javascript')

<script src="/js/docs/model.js"></script>

@endsection