<!DOCTYPE html>
<html>
    <head>
    	<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laracart</title> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.1/css/uikit.min.css">
        <link rel="stylesheet" href="/css/doc.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.1/js/uikit.min.js"></script> 
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.1/js/components/accordion.js"></script>
    </head>
    <body> 
    	@include('api.header')
		@yield('content')
  		@include('api.footer')
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.1/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.min.js"></script>
    @yield('javascript')
</html>