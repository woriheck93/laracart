<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Admin</title>
    <link rel="stylesheet" href="/css/admin/util.css" />
    <link rel="stylesheet" href="/css/admin/login.css" />
  </head>
  <body>
    <div id="root"></div>
    <script src="/js/admin/login.js"></script>
  </body>
</html>
