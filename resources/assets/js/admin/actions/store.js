import { ajaxCall } from '../util/util'
import * as ActionTypes from '../actions'

/* STORE ACTIONS START*/
function loadStart() {
  return {
    type: ActionTypes.LOAD_START
  }
}
function addStart() {
  return {
    type: ActionTypes.ADD_START
  }
}
function getStore(token) {
  return dispatch => {
    return dispatch(ajaxCall({
      url: '/api/store',
      headers: {
        Authorization: `Bearer ${token}`
      },
      dataType: 'json'
    }, loadStart, (res) => {
      return {
        type: ActionTypes.GET_STORE,
        response: res
      }
    }))
  }
}
function changeAddInput(input, value){
  return {
    type: ActionTypes.CHANGE_ADD_INPUT,
    input: input,
    value: value
  }
}
function errorAddStoreInput(errors) {
  return {
    type: ActionTypes.ERROR_ADD_STORE_INPUT,
    errors: errors
  }
}
function addStore(data, callback) {
  return dispatch => {
    return dispatch(ajaxCall({
      url: '/api/store/create',
      type: 'POST',
      dataType: 'json',
      headers: {
        Authorization: `Bearer ${data.token}`
      },
      data: {
        name: data.name,
        email: data.email,
        password: data.password,
        password_confirmation: data.password_confirmation,
        phone: data.phone,
        address: data.address,
        account: data.account,
        balance: data.balance
      }
    }, addStart, (res) => {
      if(callback) callback();
      return {
        type: ActionTypes.ADD_STORE,
        data: res
      }
    }, errorAddStoreInput))
  }
}
function changeInput(id, col, val) {
  return {
    type: ActionTypes.CHANGE_INPUT,
    id: id,
    col: col,
    val: val
  }
}
function showInput(id, col) {
  return {
    type: ActionTypes.SHOW_INPUT,
    id: id,
    col: col
  }
}
function hideInput(id, col, state) {
  return dispatch => {
    const wanted = state.store.stores[id];
    return dispatch(ajaxCall({
      url: `/api/store/${id}`,
      type: 'POST',
      dataType: 'json',
      headers: {
        Authorization: `Bearer ${state.user.token}`
      },
      data: {
        _method: 'PUT',
        name: wanted.name.value,
        phone: wanted.phone.value,
        address: wanted.address.value,
        account: wanted.account.value,
        balance: wanted.balance.value || 0
      }
    }, () => {
      return {
        type: ActionTypes.HIDE_INPUT,
        id: id,
        col: col
      }
    }, (res) => {
      return {
        type: ActionTypes.UPDATE_ROW,
        id: id,
        data: res
      }
    }));
  }
}
function showModal(){
  return {
    type: ActionTypes.SHOW_MODAL
  }
}
function hideModal(){
  return {
    type: ActionTypes.HIDE_MODAL
  }
}
/* STORE ACTIONS END*/

export { getStore, addStore, changeAddInput, changeInput, showInput, hideInput, showModal, hideModal };
