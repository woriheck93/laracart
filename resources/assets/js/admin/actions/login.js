import { ajaxCall } from '../util/util'
import * as ActionTypes from '../actions'
/*
 * action creators
 */
/* LOGIN ACTIONS START*/

function ajaxStart() {
  return {
    type: ActionTypes.REQUEST_LOGIN
  }
}
function ajaxReceive(email, response) {
  return dispatch => {
    dispatch(receiveLogin(email, response));
  }
}
function requestLogin(data) {
  return dispatch => {
    dispatch(ajaxCall({
      url: '/api/login',
      type: 'POST',
      dataType: 'json',
      data: {
        email: data.email,
        password: data.password
      }
    }, ajaxStart, response => ajaxReceive(data.email, response)));
  }
}

function receiveLogin(email, response) {
  if(response) {
    sessionStorage.setItem('token', response.data.auth_token);
    sessionStorage.setItem('email', email);
    window.location.href='/houtai/dashboard/';
    return {
      type: ActionTypes.RECEIVE_LOGIN,
      token: response.auth_token,
      email: email,
      logging: true
    }
  }
  else {
    return {
      type: ActionTypes.RECEIVE_LOGIN,
      token: null,
      email: null,
      logging: false
    }
  }
}
export default requestLogin;
/* LOGIN ACTIONS END*/
