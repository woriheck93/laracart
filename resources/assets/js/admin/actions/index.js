/**Login part start**/
export const REQUEST_LOGIN = 'REQUEST_LOGIN'
export const RECEIVE_LOGIN = 'RECEIVE_LOGIN'
/**Login part finish**/

/**Store part start**/
export const LOAD_START = 'LOAD_START'
export const ADD_START = 'ADD_START'
export const CHANGE_ADD_INPUT = 'CHANGE_ADD_INPUT'
export const CHANGE_INPUT = 'CHANGE_INPUT'
export const SHOW_INPUT = 'SHOW_INPUT'
export const HIDE_INPUT = 'HIDE_INPUT'
export const GET_STORE = 'GET_STORE'
export const ADD_STORE = 'ADD_STORE'
export const ERROR_ADD_STORE_INPUT = 'ERROR_ADD_STORE_INPUT'
export const FIXED_ADD_STORE_INPUT = 'FIXED_ADD_STORE_INPUT'
export const UPDATE_ROW = 'UPDATE_ROW'
export const DELETE_STORE = 'DELETE_STORE'
export const RECEIVE_STORE = 'RECEIVE_STORE'
export const SHOW_MODAL = 'SHOW_MODAL'
export const HIDE_MODAL = 'HIDE_MODAL'
/**Store part finish**/

/**Phone part start**/
export const GET_PHONE = 'GET_PHONE'
export const ADD_PHONE = 'ADD_PHONE'
export const UPDATE_PHONE = 'UPDATE_PHONE'
export const ERROR_ADD_PHONE_INPUT = 'ERROR_ADD_PHONE_INPUT'
export const FIXED_ADD_PHONE_INPUT = 'FIXED_ADD_PHONE_INPUT'
export const DELETE_PHONE = 'DELETE_PHONE'
export const RECEIVE_PHONE = 'RECEIVE_PHONE'
/**Phone part finish**/
