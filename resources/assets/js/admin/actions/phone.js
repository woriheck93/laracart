import { ajaxCall } from '../util/util'
import * as ActionTypes from '../actions'

/* PHONE ACTIONS START*/
function loadStart() {
  return {
    type: ActionTypes.LOAD_START
  }
}
function addStart() {
  return {
    type: ActionTypes.ADD_START
  }
}
function getPhone(token) {
  return dispatch => {
    return dispatch(ajaxCall({
      url: '/api/product/phone',
      headers: {
        Authorization: `Bearer ${token}`
      },
      dataType: 'json'
    }, loadStart, (res) => {
      return {
        type: ActionTypes.GET_PHONE,
        response: res
      }
    }))
  }
}
function changeAddInput(input, value){
  return {
    type: ActionTypes.CHANGE_ADD_INPUT,
    input: input,
    value: value
  }
}
function errorAddPhoneInput(errors) {
  return {
    type: ActionTypes.ERROR_ADD_PHONE_INPUT,
    errors: errors
  }
}
function addPhone(data, callback) {
  return dispatch => {
    return dispatch(ajaxCall({
      url: '/api/phone/create',
      type: 'POST',
      dataType: 'json',
      headers: {
        Authorization: `Bearer ${data.token}`
      },
      data: {
        name: data.name,
        memory: data.memory,
        size: data.size,
        weight: data.weight,
        color: data.color,
        capacity: data.capacity,
        base_price: data.base_price,
        camera: data.camera,
        display: data.display,
        battery: data.battery,
        operating_system: data.operating_system,
        cpu: data.cpu,
        input_output_port: data.input_output_port,
        wireless_technology: data.wireless_technology,
        remark: data.remark
      }
    }, addStart, (res) => {
      if(callback) callback();
      return {
        type: ActionTypes.ADD_PHONE,
        data: res
      }
    }, errorAddPhoneInput))
  }
}
function changeInput(id, col, val) {
  return {
    type: ActionTypes.CHANGE_INPUT,
    id: id,
    col: col,
    val: val
  }
}
function updatePhone(data, callback) {
  return dispatch => {
    return dispatch(ajaxCall({
      url: `/api/product/phone/${data.modalId}`,
      type: 'POST',
      dataType: 'json',
      headers: {
        Authorization: `Bearer ${data.token}`
      },
      data: {
        _method: 'PUT',
        name: data.name,
        base_price: data.base_price,
        color: data.color,
        size: data.size,
        weight: data.weight,
        memory: data.memory,
        camera: data.camera,
        capacity: data.capacity,
        display: data.display,
        battery: data.battery,
        operating_system: data.operating_system,
        cpu: data.cpu,
        input_output_port: data.input_output_port,
        wireless_technology: data.wireless_technology,
        remark: data.remark
      }
    }, () => {
      return {
        type: ActionTypes.ADD_START,
      }
    }, (res) => {
      if(callback) callback();
      return {
        type: ActionTypes.UPDATE_PHONE,
        res: res
      }
    }))
  }
}
function showModal(data, state){
  return dispatch => {
    switch (data.type) {
      case 'edit':
        return dispatch(ajaxCall({
          url: `/api/product/phone/${data.id}/edit`,
          type: 'GET',
          dataType: 'json',
          headers: {
            Authorization: `Bearer ${state.user.token}`
          }
        }, () => {
          return {
            type: ActionTypes.SHOW_MODAL,
            content: 'loading'
          }
        }, (res) => {
          return {
            type: ActionTypes.SHOW_MODAL,
            id: data.id,
            content: res
          }
        }));
        break;
      default:
        return {
          type: ActionTypes.SHOW_MODAL,
          content: null
        }
        break;
    }
  }
}
function hideModal(){
  return {
    type: ActionTypes.HIDE_MODAL
  }
}
/* PHONE ACTIONS END*/

export { getPhone, addPhone, changeAddInput, changeInput,updatePhone, showModal, hideModal };
