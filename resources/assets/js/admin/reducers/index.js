import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux'
import user from './user'
import store from './store'
import phone from './phone'

const rootReducer = combineReducers({
  user,
  store,
  phone,
  routing
})
export default rootReducer
