import * as ActionTypes from '../actions'
const INPUT = {
  name: {
    error: false,
    errorText: '',
    value:''
  },
  phone: {
    error: false,
    errorText: '',
    value: ''
  },
  address: {
    error: false,
    errorText: '',
    value: ''
  },
  account: {
    error: false,
    errorText: '',
    value: ''
  },
  balance: {
    error: false,
    errorText: '',
    value: ''
  },
  email: {
    error: false,
    errorText: '',
    value: ''
  }
}
const store = (state = {
  stores:{},
  logging: false,
  adding: false,
  showModal: false,
  input: INPUT
}, action) => {
  let originalStores = state.stores;
  switch (action.type) {
    case ActionTypes.LOAD_START:
      return Object.assign({}, state, {
        logging: true
      });
      break;
    case ActionTypes.ADD_START:
      return Object.assign({}, state, {
        adding: true
      });
      break;
    case ActionTypes.GET_STORE:
      var data = null,
          stores = {};
      if(action.response) {
        data = action.response.data;
        for(var store in data){
          var val = data[store];
          stores[store] = {
            id: val.id,
            name: {
              show: true,
              value: val.name || ''
            },
            phone: {
              show: true,
              value: val.phone || ''
            },
            address: {
              show: true,
              value: val.address || ''
            },
            account: {
              show: true,
              value: val.account || ''
            },
            balance: {
              show: true,
              value: val.balance || 0
            },
            email: {
              show: true,
              value: val.email || ''
            }
          }
        };
      }
      return Object.assign({}, state, {
        stores: stores,
        logging: false
      });
      break;
    case ActionTypes.ADD_STORE:
      if(action.data){
        var data = action.data.data;
        originalStores[data.id] = {
          id: data.id,
          name: {
            show: true,
            value: data.name
          },
          phone: {
            show: true,
            value: data.phone
          },
          address: {
            show: true,
            value: data.address
          },
          account: {
            show: true,
            value: data.account
          },
          balance: {
            show: true,
            value: data.balance
          },
          email: {
            show: true,
            value: data.email
          }
        };
        return Object.assign({}, state, {
          stores: originalStores,
          adding: false,
          input: INPUT
        });
      }
      else {
        return Object.assign({}, state, {
          adding: false
        });
      }
      break;
    case ActionTypes.CHANGE_ADD_INPUT:
      var input = state.input;
      input[action.input].value = action.value;
      for(var item in input) {
        input[item].error = false;
        input[item].errorText = '';
      }
      return Object.assign({}, state, {
        input: input
      });
      break;
    case ActionTypes.ERROR_ADD_STORE_INPUT:
      var input = state.input;
      action.errors.forEach((err)=>{
        input[err.item].error = true;
        input[err.item].errorText = err.content;
      });
      return Object.assign({}, state, {
        adding: false,
        input: input
      });
      break;
    case ActionTypes.FIXED_ADD_STORE_INPUT:
      var input = state.input;
      input[action.input].error = false;
      return Object.assign({}, state, {
        input: input
      });
      break;
    case ActionTypes.UPDATE_ROW:
      var { id, data } = action;
      var store = originalStores[id];
      for(var item in store) {
        if(item == 'id') continue;
        store[item].value = data.data[item];
      }
      originalStores[id] = store;
      return Object.assign({}, state, {stores: originalStores});
      break;
    case ActionTypes.DELETE_STORE:
      return state;
      break;
    case ActionTypes.RECEIVE_STORE:
      return state;
      break;
    case ActionTypes.CHANGE_INPUT:
      var { id, col, val } = action;
      originalStores[id][col] = {
        show: originalStores[id][col].show,
        value: val
      };
      return Object.assign({}, state, {stores: originalStores});
      break;
    case ActionTypes.SHOW_INPUT:
      var { id, col } = action;
      originalStores[id][col].show = false;
      return Object.assign({}, state, {stores: originalStores});
      break;
    case ActionTypes.HIDE_INPUT:
      var { id, col } = action;
      originalStores[id][col].show = true;
      return Object.assign({}, state, {stores: originalStores});
      break;
    case ActionTypes.SHOW_MODAL:
      return Object.assign({}, state, {showModal: true})
      break;
    case ActionTypes.HIDE_MODAL:
      return Object.assign({}, state, {showModal: false})
      break;
    default:
      return state;
  }
}
export default store;
