import * as ActionTypes from '../actions'
function clearInput() {
  return {
    title: '',
    name: {
      error: false,
      errorText: '',
      value:''
    },
    memory: {
      error: false,
      errorText: '',
      value: ''
    },
    size: {
      error: false,
      errorText: '',
      value: ''
    },
    weight: {
      error: false,
      errorText: '',
      value: ''
    },
    color: {
      error: false,
      errorText: '',
      value: []
    },
    capacity: {
      error: false,
      errorText: '',
      value: []
    },
    base_price: {
      error: false,
      errorText: '',
      value: 0
    },
    camera: {
      error: false,
      errorText: '',
      value: ''
    },
    display: {
      error: false,
      errorText: '',
      value: ''
    },
    battery: {
      error: false,
      errorText: '',
      value: ''
    },
    operating_system: {
      error: false,
      errorText: '',
      value: ''
    },
    cpu: {
      error: false,
      errorText: '',
      value: ''
    },
    input_output_port: {
      error: false,
      errorText: '',
      value: ''
    },
    wireless_technology: {
      error: false,
      errorText: '',
      value: ''
    },
    remark: {
      error: false,
      errorText: '',
      value: ''
    }
  };
}
const phone = (state = {
  phones:{},
  logging: false,
  adding: false,
  showModal: false,
  input: clearInput(),
  modalId: ''
}, action) => {
  let originalphones = state.phones;
  switch (action.type) {
    case ActionTypes.LOAD_START:
      return Object.assign({}, state, {
        logging: true
      });
      break;
    case ActionTypes.ADD_START:
      return Object.assign({}, state, {
        adding: true
      });
      break;
    case ActionTypes.GET_PHONE:
      var data = null,
          phones = {};
      if(action.response) {
        data = action.response.data;
        for(var phone in data){
          var val = data[phone];
          phones[phone] = {
            id: phone,
            name: val.name || '',
            memory: val.memory || '',
            size: val.size || '',
            weight: val.weight || '',
            color: val.color || [],
            capacity: val.capacity || [],
            base_price: val.base_price || 0
          }
        };
      }
      return Object.assign({}, state, {
        phones: phones,
        logging: false
      });
      break;
    case ActionTypes.ADD_PHONE:
      if(action.data){
        var data = action.data.data;
        originalphones[data.id] = {
          id: data._id,
          name: data.name,
          memory: data.memory,
          size: data.size,
          weight: data.weight,
          color: data.color,
          capacity: data.capacity,
          base_price: data.base_price
        };
        return Object.assign({}, state, {
          phones: originalphones,
          adding: false,
          input: clearInput()
        });
      }
      else {
        return Object.assign({}, state, {
          adding: false
        });
      }
      break;
    case ActionTypes.CHANGE_ADD_INPUT:
      var input = state.input;
      input[action.input].value = action.value;
      for(var item in input) {
        if(item === 'title') continue;
        input[item].error = false;
        input[item].errorText = '';
      }
      return Object.assign({}, state, { input: input });
      break;
    case ActionTypes.ERROR_ADD_PHONE_INPUT:
      var input = state.input;
      action.errors.forEach((err)=>{
        input[err.item].error = true;
        input[err.item].errorText = err.content;
      });
      return Object.assign({}, state, {
        adding: false,
        input: input
      });
      break;
    case ActionTypes.FIXED_ADD_PHONE_INPUT:
      var input = state.input;
      input[action.input].error = false;
      return Object.assign({}, state, {
        input: input
      });
      break;
    case ActionTypes.UPDATE_ROW:
      var { id, data } = action;
      var phone = originalphones[id];
      for(var item in phone) {
        if(item == 'id') continue;
        phone[item] = data.data[item];
      }
      originalphones[id] = phone;
      return Object.assign({}, state, {phones: originalphones});
      break;
    case ActionTypes.DELETE_PHONE:
      return state;
      break;
    case ActionTypes.RECEIVE_PHONE:
      return state;
      break;
    case ActionTypes.SHOW_MODAL:
      var content = action.content;
      var input = clearInput();
      if(content === 'loading'){
        return Object.assign({}, state, {showModal: true, adding: true, input: input, modalId: ''})
      }
      else if(content === null){
        input.title = 'Add Phone';
        return Object.assign({}, state, {showModal: true, adding: false, input: input, modalId: ''})
      }
      else {
        input.title = 'Edit Phone';
        for(var item in input) {
          if(item === 'title') continue;
          var data = content.data[item];
          var value = Array.isArray(data) ? data.join(', ') : data;
          input[item].value = value;
        }
        return Object.assign({}, state, {showModal: true, adding: false, input: input, modalId: action.id})
      }
      break;
    case ActionTypes.HIDE_MODAL:
      return Object.assign({}, state, {showModal: false})
      break;
    case ActionTypes.UPDATE_PHONE:
      var data = action.res.data;
      var phone = originalphones[data._id];
      phone.name= data.name;
      phone.memory= data.memory;
      phone.size= data.size;
      phone.weight= data.weight;
      phone.color= data.color.join(', ');
      phone.capacity= data.capacity.join(', ');
      phone.base_price= data.base_price;
      originalphones[data._id] = phone;
      return Object.assign({}, state, {showModal: false, phones:originalphones});
    default:
      return state;
  }
}
export default phone;
