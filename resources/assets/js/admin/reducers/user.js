import * as ActionTypes from '../actions'
let email = sessionStorage.getItem('email') ? sessionStorage.getItem('email') : '';
let token = sessionStorage.getItem('token') ? sessionStorage.getItem('token') : '';
//Update user token and email
const user = (state = { token: token, email: email, logging: false }, action) => {
  switch (action.type) {
    case ActionTypes.REQUEST_LOGIN:
      return Object.assign({}, state, {
        logging: true
      });
      break;
    case ActionTypes.RECEIVE_LOGIN:
      return Object.assign({}, state, {
        token: action.token,
        email: action.email,
        logging: action.logging
      })
      break;
    default:
      return state;
  }
}
export default user;
