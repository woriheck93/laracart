import { connect } from 'react-redux'
import Login from '../components/Login'

const mapStateToProps = (state, ownProps) => {
  return {
    logging: state.user.logging
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch: dispatch
  }
}
const LoginApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

export default LoginApp;
