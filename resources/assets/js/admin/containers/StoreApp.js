import { connect } from 'react-redux'
import Store from '../components/Store'

const mapStateToProps = (state, ownProps) => {
  return {
    state: state
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch: dispatch
  }
}
const StoreApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Store)

export default StoreApp;
