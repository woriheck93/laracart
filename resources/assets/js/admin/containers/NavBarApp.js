import { connect } from 'react-redux'
import NavBar from '../components/NavBar'

const mapStateToProps = (state, ownProps) => {
  return {
    current: ownProps.current
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch: dispatch
  }
}
const NavBarApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar)

export default NavBarApp;
