import { connect } from 'react-redux'
import Phone from '../components/Phone'

const mapStateToProps = (state, ownProps) => {
  return {
    state: state
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch: dispatch
  }
}
const PhoneApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Phone)

export default PhoneApp;
