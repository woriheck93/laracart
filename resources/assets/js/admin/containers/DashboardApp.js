import { connect } from 'react-redux'
import Dashboard from '../components/Dashboard'

const mapStateToProps = (state, ownProps) => {
  return {
    email: state.user.email,
    token: state.user.token
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch: dispatch
  }
}
const DashboardApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)

export default DashboardApp;
