import { ajax } from 'jquery';
import swal from 'sweetalert';

export const ajaxCall = (data, ajaxStart, ajaxReceive, inputErrorHandler) => {
  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.
  return dispatch => {
    // First dispatch: the app state is updated to inform
    // that the API call is starting.
    dispatch(ajaxStart())

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.

    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.

    return ajax(data)
      .then(response =>

        // We can dispatch many times!
        // Here, we update the app state with the results of the API call.
        dispatch(ajaxReceive(response))
      )
      .catch((xhr) => {
        let closeOnConfirm = true;
        let callback = ()=>{};
        let message = '';
        switch (xhr.status) {
          case 500:
            message = 'Network Error!';
            break;
          default:
            const error = xhr.responseJSON.errors;
            const code = error.code;
            message = error.message;
            if(code >= 100 && code <= 199) {
              if(window.location.pathname != '/houtai/' && window.location.pathname != '/houtai') {
                closeOnConfirm = false;
                callback = ()=>{
                  window.location.href='/houtai/';
                }
              }
            }
        }
        if(typeof message == 'object'){
          let errorItem = [];
          for(var item in message) {
            errorItem.push({
              item: item,
              content: message[item]
            });
          }
          dispatch(inputErrorHandler(errorItem));
        }
        else{
          swal({
            type: 'error',
            title: 'Opps',
            text: message,
            closeOnConfirm: closeOnConfirm
          }, ()=>{
            callback();
            dispatch(ajaxReceive(null));
          });
        }
      });

      // In a real world app, you also want to
      // catch any error in the network call.
  }
}
