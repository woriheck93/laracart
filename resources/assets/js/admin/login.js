import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, browserHistory, Route } from 'react-router'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import User from './reducers'
import LoginApp from './containers/LoginApp'

const loggerMiddleware = createLogger();

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify here name, actionsBlacklist, actionsCreators and other options
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(
    thunkMiddleware, // 允许我们 dispatch() 函数
    loggerMiddleware // 一个很便捷的 middleware，用来打印 action 日志
  )
);
let store = createStore(
  User,
  enhancer
);
ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/houtai" component={LoginApp} />
    </Router>
  </Provider>,
  document.getElementById('root')
)
