import React from 'react';
import { getPhone, addPhone, changeAddInput, changeInput, updatePhone, showModal, hideModal } from '../actions/phone';

const items = [{
  title: 'name',
  placeholder: 'Name',
  type: 'text'
},{
  title: 'base_price',
  placeholder: 'Base Price',
  type: 'number'
},{
  title: 'color',
  placeholder: 'Color',
  type: 'text'
},{
  title: 'size',
  placeholder: 'Size',
  type: 'text'
},{
  title: 'weight',
  placeholder: 'Weight',
  type: 'text'
},{
  title: 'memory',
  placeholder: 'Memory',
  type: 'text'
},{
  title: 'capacity',
  placeholder: 'Capacity',
  type: 'text'
}];
class PhoneModalInput extends React.Component {
  constructor (props) {
    super(props);
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler(input, e){
    const dispatch = this.props.dispatch;
    dispatch(changeAddInput(input, e.target.value))
  }
  render () {
    let { error, errorText, type, name, value, inputType } = this.props;
    return (
      <div className="uk-form-row">
        <label className="uk-form-label" htmlFor={`modal-${type}`}>
          {name}
          {error?<span className="warning">{errorText}</span>:<span></span>}
        </label>
        <input
          name={`modal-${type}`}
          ref={type}
          type={inputType}
          className={error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
          placeholder={`Please Enter ${name}`}
          onChange={this.changeHandler.bind(null, type)}
          value={value}
        />
      </div>
    )
  }
}
class PhoneModal extends React.Component { //新增手機modal
  constructor(props){
    super(props);
    this.hideModal = this.hideModal.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }
  hideModal(e){
    const className = e.target.className;
    const test = /(uk\-open)|(uk\-close)/;
    if(test.test(className)){
      this.props.dispatch(hideModal());
    }
  }
  submitHandler(e) {
    e.preventDefault();
    let {
      name,
      base_price,
      color,
      size,
      weight,
      memory,
      camera,
      capacity,
      display,
      battery,
      operating_system,
      cpu,
      input_output_port,
      wireless_technology,
      remark } = this.props.input;
    const { dispatch, modalId, token } = this.props;
    let data = {
      name: name.value,
      base_price: base_price.value,
      color: color.value.split(','),
      size: size.value,
      weight: weight.value,
      memory: memory.value,
      camera: camera.value,
      capacity: capacity.value.split(','),
      display: display.value,
      battery: battery.value,
      operating_system: operating_system.value,
      cpu: cpu.value,
      input_output_port: input_output_port.value,
      wireless_technology: wireless_technology.value,
      remark: remark.value,
      token: token
    };
    if(modalId){
      data.modalId = modalId;
      dispatch(updatePhone(data, ()=>{
        dispatch(hideModal());
      }));
    }
    else {
      dispatch(addPhone(data, ()=>{
        dispatch(hideModal());
      }));
    }
  }
  render () {
    if(this.props.show){
      let { input, dispatch } = this.props;
      let MODAL_INPUT = [
        { type: 'name', name: 'Name', inputType:'text' },
        { type: 'base_price', name: 'Base Price', inputType: 'number' },
        { type: 'color', name: 'Color (Seperate with comma)', inputType: 'text' },
        { type: 'size', name: 'Size', inputType: 'text' },
        { type: 'weight', name: 'Weight', inputType: 'text' },
        { type: 'memory', name: 'Memory', inputType: 'text' },
        { type: 'camera', name: 'Camera', inputType: 'text' },
        { type: 'capacity', name: 'Capacity (Seperate with comma)', inputType: 'text' },
        { type: 'display', name: 'Display', inputType: 'text' },
        { type: 'battery', name: 'Battery', inputType: 'text' },
        { type: 'operating_system', name: 'Operating System', inputType: 'text' },
        { type: 'cpu', name:'CPU', inputType: 'text' },
        { type: 'input_output_port', name: 'I/O port', inputType: 'text' },
        { type: 'wireless_technology', name: 'Wireless Support', inputType: 'text' },
        { type: 'remark', name: 'Remark', inputType: 'text' }
      ];
      let modalInput = MODAL_INPUT.map((item, index)=>{
        let currentInput = input[item.type];
        return (
          <PhoneModalInput
            key={index}
            type={item.type}
            name={item.name}
            inputType={item.inputType}
            error={currentInput.error}
            errorText={currentInput.errorText}
            value={currentInput.value}
            dispatch={dispatch}
          />
        )
      })
      let loading = this.props.adding ?
        <div className="overlay-background">
          <i className="uk-icon-spinner uk-icon-spin uk-icon-large" />
        </div> : <div></div>;
      return (
        <div>
          {loading}
          <div id="my-id" className="uk-modal uk-open" style={{display:'block'}} onClick={this.hideModal}>
            <div className="uk-modal-dialog">
              <a className="uk-modal-close uk-close" onClick={this.hideModal}></a>
              <form className="uk-form uk-form-stacked" onSubmit={this.submitHandler}>
                <legend>{input.title}</legend>
                {modalInput}
                <div className="uk-form-row">
                  <button className="uk-button uk-button-primary" type="submit" disabled={this.props.adding?true:false}>{this.props.modalId?'Update':'Submit'}</button>
                </div>
            </form>
            </div>
          </div>
        </div>
      );
    }
    else {
      return (
        <div></div>
      );
    }
  }
}
class AddPhone extends React.Component { //搜尋與新增手機bar
  constructor(props) {
    super(props);
    this.showAddModal = this.showAddModal.bind(this);
  }
  showAddModal (e) {
    e.preventDefault();
    this.props.dispatch(showModal());
  }
  render () {
    const props = this.props;
    const state = props.state;
    return (
      <div>
        <form className="uk-form uk-form-stacked">
          <div className="uk-grid">
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Name</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Name" id="name" ref="name" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="phone">Phone</label>
              <div className="uk-forms-controls">
                <input type="tel" placeholder="Phone" id="phone" ref="phone" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Address</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Address" id="address" ref="address" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Account</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Account" id="account" ref="account" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="add"> &nbsp; </label>
              <div className="uk-forms-controls">
                <div className="uk-button-group">
                  <button className="uk-button">
                    <i className="uk-icon-search"></i>
                  </button>
                  <button className="uk-button uk-button-primary" onClick={this.showAddModal}>
                    <i className="uk-icon-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <PhoneModal
          adding={state.phone.adding}
          show={state.phone.showModal}
          dispatch={props.dispatch}
          token={state.user.token}
          input={state.phone.input}
          modalType={state.phone.modalType}
          phones={state.phone.phones}
          modalId={state.phone.modalId}
        />
      </div>
    )
  }
}
class PhoneData extends React.Component { //各個手機data
  constructor(props) {
    super(props);
    this.showModal = this.showModal.bind(this);
  }
  showModal (e) {
    e.stopPropagation();
    const { state, dispatch, reactKey } = this.props;
    let data = {
      id: reactKey,
      type: 'edit'
    }
    dispatch((showModal(data, state)))
  }
  render () {
    const showState = this.props.phone;
    let list = items.map((item, index) => {
      let value = showState[item.title];
      var content = Array.isArray(value) ? value.join(', ') : value;
      return (
          <td key={item.title}>
            {content}
          </td>
      )
    })
    return (
      <tr onClick={this.showModal}>{list}</tr>
    )
  }
}
class PhoneList extends React.Component { //手機list父元素
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    const dispatch = this.props.dispatch;
    dispatch(getPhone(this.props.state.user.token));
  }
  render() {
    const phones = this.props.phones;
    let phoneData = [];
    for(var phone in phones){
      phoneData.push(<PhoneData
        phone={phones[phone]}
        key={phone}
        reactKey={phone}
        dispatch={this.props.dispatch}
        state={this.props.state} />
      );
    };
    return (
      <tbody>
        {phoneData}
      </tbody>
    )
  }
}
class Phone extends React.Component { //主要合併Component
  constructor (props) {
    super(props);
  }
  render () {
    let phone = this.props.state.phone;
    let phones = phone.phones;
    const loading = phone.logging ? <i className="uk-icon-spinner uk-icon-spin uk-icon-small" /> : <div />;
    return (
      <div id="phone">
        <AddPhone dispatch={this.props.dispatch} state={this.props.state} />
        <div className="divider"></div>
        <div className="uk-overflow-container">
          <table className="uk-table uk-table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Base Price</th>
                <th>Color</th>
                <th>Size</th>
                <th>Weight</th>
                <th>Memory</th>
                <th>Capacity</th>
              </tr>
            </thead>
            <PhoneList dispatch={this.props.dispatch} phones={phones} state={this.props.state} />
            <tfoot>
              <tr>
                <th colSpan="7" style={{textAlign:'center'}}>{loading}</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

export default Phone;
