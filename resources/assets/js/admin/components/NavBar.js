import React from 'react';
import { Link } from 'react-router';

class NavBar extends React.Component {
  constructor (props) {
    super(props);
  }
  render () {
    let current = this.props.current;
    let active = {
      dashboard: '',
      store: '',
      phone: '',
      customers: '',
      settings: ''
    };
    active[current] = 'uk-active';
    return (
      <nav className="uk-navbar">
        <ul className="uk-navbar-nav">
          <li className={active.dashboard}><Link to="/houtai/dashboard">HOME</Link></li>
          <li className={active.store}><Link to="/houtai/store">STORE</Link></li>
          <li className={active.phone}><Link to="/houtai/phone">PHONE</Link></li>
          <li className={active.customers}><Link to="/houtai/customers">CUSTOMERS</Link></li>
        </ul>
        <div className="uk-navbar-flip">
          <ul className="uk-navbar-nav">
            <li className={active.settings}><Link to="/houtai/settings"><i className="uk-icon-cog uk-icon-small"></i></Link></li>
            <li><a href=""><i className="uk-icon-sign-out uk-icon-small"></i></a></li>
          </ul>
        </div>
      </nav>
    )
  }
}

export default NavBar;
