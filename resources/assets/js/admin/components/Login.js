import React from 'react';
import requestLogin from '../actions/login';
import swal from 'sweetalert';

class Login extends React.Component {
  constructor (props) {
    super(props);
    this.submitHandler = this.submitHandler.bind(this);
  }
  submitHandler (e) {
    e.preventDefault();
    const dispatch = this.props.dispatch;
    if(!this.props.logging) {
      const { panel, email, password } = this.refs;
      const mail = email.value;
      const pword = password.value;
      if(mail.length === 0 || pword.length === 0) {
        swal({
          title: "Error!",
          text: "Username or Password cannot be empty!",
          type: "error",
          confirmButtonText: "OK"
        });
        return false;
      }
      else {
        dispatch(requestLogin({
          email: mail,
          password: pword
        }));
      }
    }
  }
  render () {
    let panelClass = "uk-panel uk-panel-box";
    let displayOverlay = { display: 'none' };
    let disabled = false;
    if(this.props.logging) {
      panelClass = "uk-panel uk-panel-box uk-overlay";
      displayOverlay = {};
      disabled = true;
    }
    return (
      <div id="login" className="uk-grid">
        <div className={panelClass}>
          <form className="uk-form uk-form-stacked" onSubmit={this.submitHandler}>
            <fieldset>
              <legend>
                <h2 className="uk-text-primary">Admin Panel</h2>
              </legend>
              <div className="uk-form-row">
                <label className="uk-form-label" htmlFor="email">Email</label>
                <div className="uk-form-controls">
                  <input type="text" placeholder="Email" id="email" ref="email" className="uk-form-width-medium" />
                </div>
              </div>
              <div className="uk-form-row">
                <label className="uk-form-label" htmlFor="password">Password</label>
                <div className="uk-form-controls">
                  <input type="password" placeholder="Password" id="password" ref="password" className="uk-form-width-medium" />
                </div>
              </div>
              <div className="uk-form-row" style={{textAlign:'center'}}>
                <button className="uk-button uk-button-primary" type="submit" disabled={disabled}>Login</button>
              </div>
            </fieldset>
          </form>
          <div className="uk-overlay-panel uk-overlay-background" style={displayOverlay}>
            <div className="loading">
              <i className="uk-icon-spinner uk-icon-spin uk-icon-medium" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login;
