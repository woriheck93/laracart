import React from 'react';
import { getStore, addStore, changeAddInput, changeInput, showInput, hideInput, showModal, hideModal } from '../actions/store';

const items = [{
  title: 'name',
  placeholder: 'Name',
  type: 'text'
},{
  title: 'phone',
  placeholder: 'Phone',
  type: 'tel'
},{
  title: 'address',
  placeholder: 'Address',
  type: 'text'
},{
  title: 'account',
  placeholder: 'Account',
  type: 'text'
},{
  title: 'balance',
  placeholder: 'Balance',
  type: 'tel'
}];
class AddStoreModal extends React.Component { //新增廠商modal
  constructor(props){
    super(props);
    this.hideModal = this.hideModal.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  hideModal(e){
    const className = e.target.className;
    const test = /(uk\-open)|(uk\-close)/;
    if(test.test(className)){
      this.props.dispatch(hideModal());
    }
  }
  generatePassword() {
    var length = 12,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
  submitHandler(e) {
    e.preventDefault();
    let { name, email, phone, address, account, balance } = this.refs;
    const dispatch = this.props.dispatch;
    const addPassword = this.generatePassword();
    let input = this.props.input;
    dispatch(addStore({
      name: input.name.value,
      email: input.email.value,
      phone: input.phone.value,
      address: input.address.value,
      account: input.account.value,
      balance: input.balance.value,
      password: addPassword,
      password_confirmation: addPassword,
      token: this.props.token
    }, ()=>{
      this.props.dispatch(hideModal());
    }));
  }
  changeHandler(input, e){
    const dispatch = this.props.dispatch;
    dispatch(changeAddInput(input, e.target.value))
  }
  render () {
    if(this.props.show){
      let loading = this.props.adding ?
        <div className="overlay-background">
          <i className="uk-icon-spinner uk-icon-spin uk-icon-large" />
        </div> : <div></div>;
      let input = this.props.input;
      return (
        <div>
          {loading}
          <div id="my-id" className="uk-modal uk-open" style={{display:'block'}} onClick={this.hideModal}>
            <div className="uk-modal-dialog">
              <a className="uk-modal-close uk-close" onClick={this.hideModal}></a>
              <form className="uk-form uk-form-stacked" onSubmit={this.submitHandler}>
                <legend>Add Store</legend>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-name">
                    Name
                    {input.name.error?<span className="warning">{input.name.errorText}</span>:<span></span>}
                  </label>
                  <input
                    name="add-name"
                    ref="name"
                    type="text"
                    className={input.name.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter name"
                    onChange={this.changeHandler.bind(null, 'name')}
                    value={input.name.value}
                  />
                </div>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-email">
                    Email
                    {input.email.error?<span className="warning">{input.email.errorText}</span>:<span></span>}
                  </label>
                  <input
                    name="add-email"
                    ref="email"
                    type="email"
                    className={input.email.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter email"
                    onChange={this.changeHandler.bind(null, 'email')}
                    value={input.email.value}
                  />
                </div>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-phone">Phone</label>
                  <input
                    name="add-phone"
                    ref="phone"
                    type="tel"
                    className={input.phone.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter phone no."
                    onChange={this.changeHandler.bind(null, 'phone')}
                    value={input.phone.value}
                  />
                </div>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-address">Address</label>
                  <input
                    name="add-address"
                    ref="address"
                    type="text"
                    className={input.address.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter address"
                    onChange={this.changeHandler.bind(null, 'address')}
                    value={input.address.value}
                  />
                </div>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-account">Account</label>
                  <input
                    name="add-account"
                    ref="account"
                    type="text"
                    className={input.account.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter account no."
                    onChange={this.changeHandler.bind(null, 'account')}
                    value={input.account.value}
                  />
                </div>
                <div className="uk-form-row">
                  <label className="uk-form-label" htmlFor="add-email">Balance</label>
                  <input
                    name="add-balance"
                    ref="balance"
                    type="number"
                    className={input.balance.error?'uk-width-1-1 uk-form-danger':'uk-width-1-1'}
                    placeholder="Please enter balance"
                    onChange={this.changeHandler.bind(null, 'balance')}
                    value={input.balance.value}
                  />
                </div>
                <div className="uk-form-row">
                  <button className="uk-button uk-button-primary" type="submit" disabled={this.props.adding?true:false}>Submit</button>
                </div>
            </form>
            </div>
          </div>
        </div>
      );
    }
    else {
      return (
        <div></div>
      );
    }
  }
}
class AddStore extends React.Component { //搜尋與新增廠商bar
  constructor(props) {
    super(props);
    this.showAddModal = this.showAddModal.bind(this);
  }
  showAddModal (e) {
    e.preventDefault();
    this.props.dispatch(showModal());
  }
  render () {
    const props = this.props;
    const state = props.state;
    return (
      <div>
        <form className="uk-form uk-form-stacked">
          <div className="uk-grid">
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Name</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Name" id="name" ref="name" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="phone">Phone</label>
              <div className="uk-forms-controls">
                <input type="tel" placeholder="Phone" id="phone" ref="phone" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Address</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Address" id="address" ref="address" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="name">Account</label>
              <div className="uk-forms-controls">
                <input type="text" placeholder="Account" id="account" ref="account" />
              </div>
            </div>
            <div className="uk-width-1-6">
              <label className="uk-form-label" htmlFor="add"> &nbsp; </label>
              <div className="uk-forms-controls">
                <div className="uk-button-group">
                  <button className="uk-button">
                    <i className="uk-icon-search"></i>
                  </button>
                  <button className="uk-button uk-button-primary" onClick={this.showAddModal}>
                    <i className="uk-icon-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <AddStoreModal
          adding={state.store.adding}
          show={state.store.showModal}
          dispatch={props.dispatch}
          token={state.user.token}
          input={state.store.input}
        />
      </div>
    )
  }
}
class StoreData extends React.Component { //各個廠商data
  constructor(props) {
    super(props);
    this.showInput = this.showInput.bind(this);
    this.hideInput = this.hideInput.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
  }
  showInput (type, e) {
    e.stopPropagation();
    const { dispatch, reactKey } = this.props;
    const thisDOM = e.target;
    const width = thisDOM.offsetWidth;
    new Promise(
      function(resolve){
        resolve(dispatch((showInput(reactKey, type))))
      }
    )
    .then(() => {
      let input = this.refs.input;
      input.style.width = `${width}px`;
      input.focus();
    });
  }
  hideInput (type, e) {
    e.stopPropagation();
    const { state, dispatch, reactKey } = this.props;
    dispatch(hideInput(reactKey, type, state));
  }
  changeHandler(type, e){
    const { state, dispatch, reactKey } = this.props;
    let value = e.target.value;
    if(type == 'balance') {
      value = Number(value);
      if(isNaN(value)){
        var tmp = state.store.stores.filter((store)=>{
          return store.id == reactKey;
        });
        value = tmp[0].balance.value;
      }
    }
    dispatch(changeInput(reactKey, type, value));
  }
  render () {
    const showState = this.props.store;
    let list = items.map((item, index) => {
      return (
        showState[item.title].show ?
          <td key={item.title} onClick={this.showInput.bind(null, item.title)}>
            <div>
              {showState[item.title].value}
            </div>
          </td> :
          <td key={item.title}>
            <div className="uk-form">
              <form onSubmit={this.hideInput.bind(null, item.title)}>
                <input
                  ref="input"
                  value={showState[item.title].value}
                  type={item.type}
                  onChange={this.changeHandler.bind(null, item.title)}
                  onBlur={this.hideInput.bind(null, item.title)}
                />
              </form>
            </div>
          </td>
      )
    })
    return (
      <tr>{list}</tr>
    )
  }
}
class StoreList extends React.Component { //廠商list父元素
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    const dispatch = this.props.dispatch;
    dispatch(getStore(this.props.state.user.token));
  }
  render() {
    const stores = this.props.stores;
    let storeData = [];
    for(var store in stores){
      storeData.push(<StoreData
        store={stores[store]}
        key={store}
        reactKey={store}
        dispatch={this.props.dispatch}
        state={this.props.state} />
      );
    };
    return (
      <tbody>
        {storeData}
      </tbody>
    )
  }
}
class Store extends React.Component { //主要合併Component
  constructor (props) {
    super(props);
  }
  render () {
    let store = this.props.state.store;
    let stores = store.stores;
    const loading = store.logging ? <i className="uk-icon-spinner uk-icon-spin uk-icon-small" /> : <div />;
    return (
      <div id="store">
        <AddStore dispatch={this.props.dispatch} state={this.props.state} />
        <div className="divider"></div>
        <div className="uk-overflow-container">
          <table className="uk-table uk-table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Account</th>
                <th>Balance</th>
              </tr>
            </thead>
            <StoreList dispatch={this.props.dispatch} stores={stores} state={this.props.state} />
            <tfoot>
              <tr>
                <th colSpan="5" style={{textAlign:'center'}}>{loading}</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

export default Store;
