import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, browserHistory, Route } from 'react-router'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import User from './reducers'
import NavBarApp from './containers/NavBarApp'
import DashboardApp from './containers/DashboardApp'
import StoreApp from './containers/StoreApp'
import PhoneApp from './containers/PhoneApp'

const loggerMiddleware = createLogger();

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify here name, actionsBlacklist, actionsCreators and other options
    }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(
    thunkMiddleware, // 允许我们 dispatch() 函数
    loggerMiddleware // 一个很便捷的 middleware，用来打印 action 日志
  )
);
let store = createStore(
  User,
  enhancer
)
class App extends React.Component {
  constructor(props){
    super(props);
  }
  render () {
    return (
      <div>
        <NavBarApp current={this.props.children.props.route.path} />
        <div className="uk-panel uk-panel-box" id="main">
          {this.props.children}
        </div>
      </div>
    )
  }
}
ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/houtai" component={App}>
        <Route path="dashboard" component={DashboardApp} />
        <Route path="store" component={StoreApp} />
        <Route path="phone" component={PhoneApp} />
        <Route path="customers" component={DashboardApp} />
        <Route path="settings" component={DashboardApp} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
