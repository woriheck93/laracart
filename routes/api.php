<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::group(['namespace' => 'Api'], function(){ 
 
 	Route::group(['namespace' => 'User'], function(){ 
		Route::get('/store',['uses' => 'StoreController@index', 'as' => 'api.store.index']);
		Route::get('/store/{store}/edit',['uses' => 'StoreController@edit', 'as' => 'api.store.edit']);
		Route::post('/store/create',['uses' => 'StoreController@store', 'as' => 'api.store.store']);
		Route::put('/store/{store}',['uses' => 'StoreController@update', 'as' => 'api.store.update']);
	});

	Route::group(['namespace' => 'Product','prefix' => 'product'], function(){
		Route::get('/phone',['uses' => 'PhoneController@index', 'as' => 'api.phone.index']);
		Route::get('/phone/{id}/edit',['uses' => 'PhoneController@edit', 'as' => 'api.phone.edit']);
		Route::post('/phone/create',['uses' => 'PhoneController@store', 'as' => 'api.phone.store']);
		Route::put('/phone/{id}',['uses' => 'PhoneController@update', 'as' => 'api.phone.update']);
	});	

});

Route::post('/login',['uses' => 'Auth\LoginController@login', 'as' => 'api.user.login']);

/*=============================================
=           	Api Documentation    	      =
=============================================*/

Route::group(['namespace' => 'Api'], function(){ 
	Route::get('document','DocumentationController@index');
	Route::get('document/{id}','DocumentationController@show');
});


Route::get('/doc', function () {
    return view('api.index');
});
Route::get('/http', function () {
    return view('api.http');
});
Route::get('/error', function () {
    return view('api.error');
});

/*============ Api Documentation  ===========*/
