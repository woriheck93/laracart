<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 
Route::group(['prefix' => '/houtai'], function () {
    
    Route::get('{any}', function ($any) {
    	return view('admin.index');
    })->where('any', '.*');

    Route::get('/', function () {
	    return view('admin.login');
	}); 

}); 

